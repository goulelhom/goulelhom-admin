const mix = require('laravel-mix');
const LiveReloadPlugin = require('webpack-livereload-plugin');

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const WebpackRTLPlugin = require('webpack-rtl-plugin');

let extractPlugins = [];
let vueExtractPlugin;

if (Config.extractVueStyles) {
    let fileName = typeof(Config.extractVueStyles) === "string" ? Config.extractVueStyles : 'vue-styles.css';
    let filePath = fileName.replace(Config.publicPath, '').replace(/^\//, "");
    vueExtractPlugin = extractPlugins.length ? extractPlugins[0] : new ExtractTextPlugin(filePath);
}

mix
    .extract([
        'animejs',
        'axios',
        'chart.js',
        'element-ui',
        'leaflet',
        'leaflet.locatecontrol',
        'leaflet.markercluster',
        'moment',
        'randomcolor',
        'tiny-cookie',
        'twix',
        'vue',
        'vue-analytics',
        'vue-awesome-swiper',
        'vue-chartjs',
        'vue-data-tables',
        'vue-flickity',
        'vue-quill-editor',
        'vue-router',
        'vue2-leaflet',
        'vue2-leaflet-markercluster',
        'vuex'
    ])
    .copy('resources/assets/images', 'public/images')
    .js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .webpackConfig({
        plugins: [
            new LiveReloadPlugin({port: 35730}),
            new WebpackRTLPlugin('/public/css/[name].rtl.css'),
        ],
    })
    .disableNotifications();
