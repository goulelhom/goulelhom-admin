# GOULELHOM Admin dashboard

## Installation

```
$ composer install
$ npm install
$ php artisan key:generate
$ php artisan migrate
$ php artisan passport:install
```

## @TODO

- Create authentication module
    - [ ] Create authentication service
    - [ ] Create authentication pages
    - [ ] Handle API authentication

- [ ] Create users module
    - [ ] Create users service

- [x] Create municipalities module

- [ ] Create claims module

- [ ] Create configuration module 

- [ ] Adapt the application to handle elasticsearch 6.* (only one type is allowed by index)
    - [ ] choose between handling the connection between
        - php / elasticsearch OR 
        - mysql / elsaticsearch OR 
        - RabbitMQ / elasticsearch