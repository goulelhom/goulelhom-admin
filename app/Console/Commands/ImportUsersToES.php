<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Sleimanx2\Plastic\Facades\Plastic;

class ImportUsersToES extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportUsersToES {offset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import users to elastic search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = $this->argument('offset');
        $limit = 300;
        $this->processJobs($offset, $limit);
        $offset += $limit;
        $this->call('ImportUsersToES', ['offset' => $offset]);
    }

    protected function processJobs($offset, $limit)
    {

        $records = User::offset($offset)->limit($limit)->get();

        if(count($records))
        {
            Plastic::persist()->bulkSave($records);
            $max = $offset + $limit;
            $this->info("Indexed users to ES  from {$offset} to {$max}.");
            unset($records);
        }else {
            $this->info("Operation finished");
            exit();
        }
    }
}
