<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 09/02/18
 * Time: 13:44
 */

namespace App\Traits\Searchables;


trait UserSearch
{
    public $searchable = ['id', 'name', 'email',];

    public function buildDocument()
    {

        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];
        return $data;
    }
}