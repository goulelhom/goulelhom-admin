<?php

namespace App;

use App\Traits\Searchables\UserSearch;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Sleimanx2\Plastic\Searchable;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $rememberTokenName
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    // suse Searchable, UserSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
