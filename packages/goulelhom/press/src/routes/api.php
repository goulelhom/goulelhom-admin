<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {

    Route::group([
        'prefix' => 'v1',
        'middleware' => 'auth:api'
    ], function () {
        Route::apiResources([
            'reviews' => \Goulelhom\Press\App\Http\Controllers\Resources\PressReviewController::class,
        ], [
            'except' => ['create', 'edit',]
        ]);
    });
});