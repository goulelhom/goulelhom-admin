<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 22/02/18
 * Time: 22:23
 */

namespace Goulelhom\Press;


use Goulelhom\Core\BaseServiceProvider;

class PressServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;
}