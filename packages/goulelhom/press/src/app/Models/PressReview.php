<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 22/02/18
 * Time: 22:27
 */

namespace Goulelhom\Press\App\Models;


use Illuminate\Database\Eloquent\Model;

class PressReview extends Model
{
    protected $table = 'press_reviews';
    protected $fillable = ['url'];
}