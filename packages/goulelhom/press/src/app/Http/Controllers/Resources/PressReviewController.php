<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 23/02/18
 * Time: 01:41
 */

namespace Goulelhom\Press\App\Http\Controllers\Resources;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Press\App\Http\Requests\PressReviewStoreRequest;
use Goulelhom\Press\App\Models\PressReview;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\Resource;

class PressReviewController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(Resource::collection(PressReview::all()));
    }

    public function store(PressReviewStoreRequest $request)
    {
        return $this->jsonResponse(new Resource(PressReview::create([
            'url' => $request->get('url'),
        ])));
    }

    public function show($id)
    {
        return $this->jsonResponse(new Resource(PressReview::findOrFail($id)));
    }

    public function update(PressReviewStoreRequest $request, $id)
    {
        $record = PressReview::findOrFail($id);
        $record->update([
            'url' => $request->get('url'),
        ]);
        return $this->jsonResponse(new Resource($record));
    }

    public function destroy($id)
    {
        PressReview::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}