<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 23/02/18
 * Time: 01:44
 */

namespace Goulelhom\Press\App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PressReviewStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|url|min:4|max:255',
        ];
    }
}