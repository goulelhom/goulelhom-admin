<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {

    Route::group([
        'prefix' => 'v1',
        'middleware' => 'auth:api'
    ], function () {
        Route::apiResources([
            'petitions' => \Goulelhom\Petitions\App\Http\Controllers\PetitionController::class,
        ], [
            'except' => ['create', 'edit',]
        ]);
    });
});