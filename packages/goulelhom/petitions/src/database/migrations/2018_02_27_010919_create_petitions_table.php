<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petitions', function (Blueprint $table) {
            $table->increments('id');

            $table->date('start_date');
            $table->date('end_date');

            $table->integer('theme_id')->unsigned();
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('cascade');

            $table->integer('petitioner_id')->unsigned();
            $table->foreign('petitioner_id')->references('id')->on('petitioners')->onDelete('cascade');

            $table->integer('target_id')->unsigned();
            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');

            $table->string('name');
            $table->text('description');
            $table->string('requested_signatures_number');

            $table->enum('status', ['launched', 'finished', 'treated', 'archived'])->default('launched');

            $table->boolean('is_boosted')->default(false);
            $table->uuid('uuid');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petitions');
    }
}
