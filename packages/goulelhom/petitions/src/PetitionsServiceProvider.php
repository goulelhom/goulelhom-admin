<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 02:02
 */

namespace Goulelhom\Petitions;


use Goulelhom\Core\BaseServiceProvider;

class PetitionsServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;

    function boot()
    {
        parent::boot();
    }
}