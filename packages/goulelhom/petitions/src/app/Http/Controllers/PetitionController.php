<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 23/03/18
 * Time: 12:28
 */

namespace Goulelhom\Petitions\App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use Carbon\Carbon;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Goulelhom\Petitions\App\Models\Petition;
use Goulelhom\Resources\App\Http\Resources\PetitionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Request;

class PetitionController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(PetitionResource::collection(Petition::orderBy('start_date', 'asc')->get()));
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new Resource(Petition::findOrFail($id)));
    }

    public function destroy($id)
    {
        Petition::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function storeAttachment($id, Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000'
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new AttachmentsResource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $id,
                'attachable_type' => Event::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }

    public function destroyAttachment(Attachment $attachment)
    {
        return (string) $attachment->delete();
    }
}