<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 02:32
 */

namespace Goulelhom\Petitions\App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $phone_number
 * @property string $mail
 * @property string $address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Petition[] $petitions
 * @property Signature[] $signatures
 */
class Target extends Model
{
    protected $table = 'targets';
    protected $fillable = ['name_en', 'name_fr', 'name_ar', 'phone_number', 'mail', 'address'];

    public function petitions()
    {
        return $this->hasMany(Petition::class);
    }

    public function signatures()
    {
        return $this->hasMany(Signature::class);
    }
}