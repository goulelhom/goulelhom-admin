<?php

namespace Goulelhom\Petitions\App\Models;


use App\Models\Attachment;
use Goulelhom\Claims\App\Models\Theme;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property string $name
 * @property string $description
 * @property int $requested_signatures_number
 * @property string $status
 * @property boolean $is_boosted
 * @property string $uuid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property mixed $signatures
 * @property mixed $target
 * @property mixed $theme
 * @property mixed $petitioner
 * @property Attachment[] $attachments
 */
class Petition extends Model
{
    use Notifiable;

    protected $table = 'petitions';
    protected $fillable = [
        'start_date',
        'end_date',
        'theme_id',
        'petitioner_id',
        'target_id',
        'name',
        'description',
        'requested_signatures_number',
        'status',
        'is_boosted',
        'uuid',
    ];

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',

    ];

    public function petitioner()
    {
        return $this->belongsTo(Petitioner::class);
    }

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function signatures()
    {
        return $this->hasMany(Signature::class);
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function haveReachedSignaturesNumber()
    {
        return $this->requested_signatures_number <= $this->signatures->count();
    }
}