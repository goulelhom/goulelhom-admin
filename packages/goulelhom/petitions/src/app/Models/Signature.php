<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 02:32
 */

namespace Goulelhom\Petitions\App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Petition $petition
 * @property Petitioner $petitioner
 */
class Signature extends Model
{
    protected $table = 'signatures';
    protected $fillable = ['petition_id', 'petitioner_id'];

    public function petition()
    {
        return $this->belongsTo(Petition::class);
    }

    public function petitioner()
    {
        return $this->belongsTo(Petitioner::class);
    }
}