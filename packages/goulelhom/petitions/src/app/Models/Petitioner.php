<?php

namespace Goulelhom\Petitions\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Petitioner
 * @package Goulelhom\Petitions\App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $phone_number
 * @property string $mail
 * @property string $address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Petition[] $petitions
 * @property Signature[] $signatures
 */
class Petitioner extends Model
{
    protected $table = 'petitioners';
    protected $fillable = ['name', 'phone_number', 'mail', 'address'];

    public function petitions()
    {
        return $this->hasMany(Petition::class);
    }

    public function signatures()
    {
        return $this->hasMany(Signature::class);
    }
}