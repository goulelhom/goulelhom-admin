<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipalities', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_en');
            $table->string('name_fr');
            $table->string('name_ar');

            $table->integer('population')->nullable();
            $table->integer('houses')->nullable();
            $table->integer('regional_council_number')->nullable();
            $table->integer('municipal_council_number')->nullable();

            $table->string('website', 80)->nullable();
            $table->string('phone', 80)->nullable();
            $table->string('email', 80)->nullable();
            $table->string('fax', 80)->nullable();

            $table->text('description_en')->nullable();
            $table->text('description_fr')->nullable();
            $table->text('description_ar')->nullable();

            $table->boolean('is_active')->default(false);

            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipalities');
    }
}
