<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Goulelhom\Localization\App\Models\Country::create([
            'name_en' => 'Tunisia',
            'name_fr' => 'Tunisie',
            'name_ar' => 'تونس'
        ]);
    }
}
