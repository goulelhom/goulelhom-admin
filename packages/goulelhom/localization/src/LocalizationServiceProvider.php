<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 01:47
 */

namespace Goulelhom\Localization;


use Goulelhom\Core\BaseServiceProvider;
use Goulelhom\Localization\App\Console\Commands\ImportCountriesToES;

class LocalizationServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;

    function boot()
    {
        parent::boot();
    }

    public function register()
    {
        $this->registerCommands();
    }

    protected function registerCommands()
    {
        $this->commands([
            // ImportCountriesToES::class,
        ]);
    }
}