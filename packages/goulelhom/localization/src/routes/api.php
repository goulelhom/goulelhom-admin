<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {

    Route::group([
        'prefix' => 'v1',
        'middleware' => 'auth:api'
    ], function () {
        Route::apiResources([
            'countries' => \Goulelhom\Localization\App\Http\Controllers\API\Resources\CountryController::class,
            'cities' => \Goulelhom\Localization\App\Http\Controllers\API\Resources\CityController::class,
            'municipalities' => \Goulelhom\Localization\App\Http\Controllers\API\Resources\MunicipalityController::class,
        ], [
            'except' => ['create', 'edit',]
        ]);
        Route::post('municipalities/upload', '\Goulelhom\Localization\App\Http\Controllers\API\Resources\MunicipalityController@storeAttachment');
        Route::delete('municipalities/upload', '\Goulelhom\Localization\App\Http\Controllers\API\Resources\MunicipalityController@destroyAttachment');
    });
});