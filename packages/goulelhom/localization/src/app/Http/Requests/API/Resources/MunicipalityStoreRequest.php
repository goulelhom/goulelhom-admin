<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 03:50
 */

namespace Goulelhom\Localization\App\Http\Requests\API\Resources;


use Illuminate\Foundation\Http\FormRequest;

class MunicipalityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|min:4|max:255',
            'name_fr' => 'required|min:4|max:255',
            'name_ar' => 'required|min:4|max:255',
            'city_id' => 'required|exists:cities,id',
        ];
    }
}