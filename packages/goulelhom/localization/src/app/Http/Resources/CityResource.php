<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 11:46
 */

namespace Goulelhom\Localization\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class CityResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
            'country_id' => $this->country_id,
            'country' => $this->country,
            'population' => $this->population(),
            'houses' => $this->houses(),
        ];
    }
}