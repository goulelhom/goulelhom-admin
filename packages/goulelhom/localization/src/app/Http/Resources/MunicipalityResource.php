<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 01/02/18
 * Time: 02:18
 */

namespace Goulelhom\Localization\App\Http\Resources;


use Goulelhom\Claims\App\Http\Resources\ClaimPublicResource;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Illuminate\Http\Resources\Json\Resource;

class MunicipalityResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
            'population' => $this->population,
            'houses' => $this->houses,
            'regional_council_number' => $this->regional_council_number,
            'municipal_council_number' => $this->municipal_council_number,
            'website' => $this->website,
            'phone' => $this->phone,
            'email' => $this->email,
            'fax' => $this->fax,
            'description_en' => $this->description_en,
            'description_fr' => $this->description_fr,
            'description_ar' => $this->description_ar,
            'is_active' => $this->is_active === 1,
            'city_id' => $this->city_id,

            'claims' => ClaimPublicResource::collection($this->claims),

            'attachments' => AttachmentsResource::collection($this->attachments),
            'cover' => new AttachmentsResource($this->attachments()->first()),
            'city' => new CityResource($this->city),
        ];
    }
}