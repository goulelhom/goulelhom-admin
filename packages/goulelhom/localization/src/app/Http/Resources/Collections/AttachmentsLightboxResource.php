<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 01/02/18
 * Time: 02:27
 */

namespace Goulelhom\Localization\App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\Resource;

class AttachmentsLightboxResource extends Resource
{
    public function toArray($request)
    {
        return [
            'title' => $this->attachable_id,
            'src' => config('app.url') . $this->url,
        ];
    }
}