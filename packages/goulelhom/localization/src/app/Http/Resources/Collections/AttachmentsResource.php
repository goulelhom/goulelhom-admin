<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 01/02/18
 * Time: 02:27
 */

namespace Goulelhom\Localization\App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\Resource;

class AttachmentsResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'attachable_id' => $this->attachable_id,
            'name' => $this->filename,
            'mime' => $this->mime,
            'size' => $this->size,
            'url' => $this->url,
        ];
    }
}