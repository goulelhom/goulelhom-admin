<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 14:57
 */

namespace Goulelhom\Localization\App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\Resource;

class MunicipalitiesResource extends Resource
{
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
            'population' => $this->population,
            'houses' => $this->houses,
            'regional_council_number' => $this->regional_council_number,
            'municipal_council_number' => $this->municipal_council_number,
            'website' => $this->website,
            'phone' => $this->phone,
            'email' => $this->email,
            'fax' => $this->fax,
            'description_en' => $this->description_en,
            'description_fr' => $this->description_fr,
            'description_ar' => $this->description_ar,
            'is_active' => $this->is_active,
            'city_id' => $this->city_id,
            'city' => $this->city,
            'cover' => new AttachmentsResource($this->attachments()->first()),
            'created_at' => (string) $this->created_at,
        ];
    }
}