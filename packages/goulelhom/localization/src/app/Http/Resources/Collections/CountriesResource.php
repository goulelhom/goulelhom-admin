<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 21:56
 */

namespace Goulelhom\Localization\App\Http\Resources\Collections;


use Illuminate\Http\Resources\Json\Resource;

class CountriesResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
            'cities' => $this->cities,
        ];
    }
}