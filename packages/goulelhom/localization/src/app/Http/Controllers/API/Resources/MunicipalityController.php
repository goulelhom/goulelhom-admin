<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 14:53
 */

namespace Goulelhom\Localization\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Localization\App\Http\Requests\API\Resources\MunicipalityStoreRequest;
use Goulelhom\Localization\App\Http\Resources\Collections\MunicipalitiesResource;
use Goulelhom\Localization\App\Http\Resources\MunicipalityResource;
use Goulelhom\Localization\App\Models\Municipality;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class MunicipalityController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(MunicipalitiesResource::collection(Municipality::all()));
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new MunicipalityResource(Municipality::findOrFail($id)));
    }

    public function store(MunicipalityStoreRequest $request)
    {
        return $this->jsonResponse(new MunicipalityResource(Municipality::create([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'population' => $request->get('population'),
            'houses' => $request->get('houses'),
            'regional_council_number' => $request->get('regional_council_number'),
            'municipal_council_number' => $request->get('municipal_council_number'),
            'website' => $request->get('website'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'fax' => $request->get('fax'),
            'description_en' => $request->get('description_en'),
            'description_fr' => $request->get('description_fr'),
            'description_ar' => $request->get('description_ar'),
            'is_active' => $request->get('is_active'),
            'city_id' => $request->get('city_id'),
        ])));
    }

    public function update(MunicipalityStoreRequest $request, $id)
    {
        $municipality = Municipality::findOrFail($id);
        $municipality->update([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'population' => $request->get('population'),
            'houses' => $request->get('houses'),
            'regional_council_number' => $request->get('regional_council_number'),
            'municipal_council_number' => $request->get('municipal_council_number'),
            'website' => $request->get('website'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'fax' => $request->get('fax'),
            'description_en' => $request->get('description_en'),
            'description_fr' => $request->get('description_fr'),
            'description_ar' => $request->get('description_ar'),
            'is_active' => $request->get('is_active'),
            'city_id' => $request->get('city_id'),
        ]);
        return $this->jsonResponse(new MunicipalityResource($municipality));
    }

    public function destroy($id)
    {
        Municipality::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function storeAttachment(Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000',
            'attachable_id' => 'required|integer'
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new Resource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $request->get('attachable_id'),
                'attachable_type' => Municipality::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }

    public function destroyAttachment(Attachment $attachment)
    {
        return (string) $attachment->delete();
    }
}