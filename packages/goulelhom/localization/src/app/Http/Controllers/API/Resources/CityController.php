<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 02:05
 */

namespace Goulelhom\Localization\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Localization\App\Http\Requests\API\Resources\CityStoreRequest;
use Goulelhom\Localization\App\Http\Resources\CityResource;
use Goulelhom\Localization\App\Models\City;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\Resource;

class CityController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(CityResource::collection(City::all()));
    }

    public function store(CityStoreRequest $request)
    {
        return $this->jsonResponse(new CityResource(City::create([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'country_id' => $request->get('country_id'),
        ])));
    }

    public function show($id)
    {
        return $this->jsonResponse(new CityResource(City::findOrFail($id)));
    }

    public function update(CityStoreRequest $request, $id)
    {
        $city = City::findOrFail($id);
        $city->update([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'country_id' => $request->get('country_id'),
        ]);
        return $this->jsonResponse(new CityResource($city));
    }

    public function destroy($id)
    {
        City::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}