<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 02:05
 */

namespace Goulelhom\Localization\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Localization\App\Http\Requests\API\Resources\CountryStoreRequest;
use Goulelhom\Localization\App\Http\Resources\Collections\CountriesResource;
use Goulelhom\Localization\App\Models\Country;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class CountryController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(CountriesResource::collection(Country::all()));
    }

    public function store(CountryStoreRequest $request)
    {
        return $this->jsonResponse(new Resource(Country::create([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
        ])));
    }

    public function show($id)
    {
        return $this->jsonResponse(new Resource(Country::findOrFail($id)));
    }

    public function update(CountryStoreRequest $request, $id)
    {
        $country = Country::findOrFail($id);
        $country->update([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
        ]);
        return $this->jsonResponse(new Resource($country));
    }

    public function destroy($id)
    {
        Country::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}