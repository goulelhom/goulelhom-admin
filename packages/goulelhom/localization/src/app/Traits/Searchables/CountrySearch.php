<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 09/02/18
 * Time: 16:51
 */

namespace Goulelhom\Localization\App\Traits;


trait CountrySearch
{
    public $searchable = ['id', 'name_en', 'name_fr', 'name_ar'];

    public function buildDocument()
    {

        $data = [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
        ];
        return $data;
    }
}