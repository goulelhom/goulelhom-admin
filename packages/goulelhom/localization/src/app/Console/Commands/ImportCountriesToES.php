<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 09/02/18
 * Time: 18:19
 */

namespace Goulelhom\Localization\App\Console\Commands;


use Goulelhom\Localization\App\Models\Country;
use Illuminate\Console\Command;
use Sleimanx2\Plastic\Facades\Plastic;

class ImportCountriesToES extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportCountriesToES {offset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import countries to elastic search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = $this->argument('offset');
        $limit = 300;
        $this->processJobs($offset, $limit);
        $offset += $limit;
        $this->call('ImportCountriesToES', ['offset' => $offset]);
    }

    protected function processJobs($offset, $limit)
    {

        $records = Country::offset($offset)->limit($limit)->get();

        if(count($records))
        {
            Plastic::persist()->bulkSave($records);
            $max = $offset + $limit;
            $this->info("Indexed countries to ES  from {$offset} to {$max}.");
            unset($records);
        }else {
            $this->info("Operation finished");
            exit();
        }
    }
}