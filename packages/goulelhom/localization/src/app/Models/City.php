<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 02:11
 */

namespace Goulelhom\Localization\App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_ar
 * @property int $country_id
 * @property Country $country
 * @property Municipality[] $municipalities
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class City extends Model
{
    protected $table = 'cities';
    protected $fillable = ['name_en', 'name_fr', 'name_ar', 'country_id'];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function municipalities()
    {
        return $this->hasMany(Municipality::class, 'city_id');
    }

    public function population()
    {
        return $this->municipalities()->sum('population');
    }

    public function houses()
    {
        return $this->municipalities()->sum('houses');
    }
}