<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 14:21
 */

namespace Goulelhom\Localization\App\Models;


use App\Models\Attachment;
use Goulelhom\Claims\App\Models\Claim;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_ar
 * @property int $population
 * @property int $houses
 * @property int regional_council_number
 * @property int municipal_council_number
 * @property string website
 * @property string phone
 * @property string email
 * @property string fax
 * @property string description_en
 * @property string description_fr
 * @property string description_ar
 * @property bool is_active
 * @property int city_id
 * @property City $city
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Attachment[] $attachments
 */
class Municipality extends Model
{
    protected $table = 'municipalities';
    protected $fillable = [
        'name_en',
        'name_fr',
        'name_ar',
        'population',
        'houses',
        'regional_council_number',
        'municipal_council_number',
        'website',
        'phone',
        'email',
        'fax',
        'description_en',
        'description_fr',
        'description_ar',
        'is_active',
        'city_id'
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function claims()
    {
        return $this->hasMany(Claim::class);
    }
}