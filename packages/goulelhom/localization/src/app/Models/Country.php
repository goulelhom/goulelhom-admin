<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 01:56
 */

namespace Goulelhom\Localization\App\Models;


use Goulelhom\Localization\App\Traits\CountrySearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Sleimanx2\Plastic\Searchable;

/**
 * @property int $id
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_ar
 * @property City[] $cities
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Country extends Model
{
    use Notifiable;
    // use Searchable, CountrySearch;

    protected $table = 'countries';
    protected $fillable = ['name_en', 'name_fr', 'name_ar'];

    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }
}