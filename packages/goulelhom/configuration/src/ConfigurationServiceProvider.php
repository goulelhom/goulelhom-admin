<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 02/02/18
 * Time: 15:58
 */

namespace Goulelhom\Configuration;


use Goulelhom\Core\BaseServiceProvider;

class ConfigurationServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;

    function boot()
    {
        parent::boot();
    }
}