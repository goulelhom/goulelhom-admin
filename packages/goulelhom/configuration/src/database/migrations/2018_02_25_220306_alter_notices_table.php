<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notices', function (Blueprint $table) {
            // $table->mediumText('content_en')->nullable()->change();
            // $table->mediumText('content_fr')->nullable()->change();
            // $table->mediumText('content_ar')->nullable()->change();
            DB::statement('ALTER TABLE `notices` MODIFY `content_en`  MEDIUMTEXT;');
            DB::statement('ALTER TABLE `notices` MODIFY `content_fr`  MEDIUMTEXT;');
            DB::statement('ALTER TABLE `notices` MODIFY `content_ar`  MEDIUMTEXT;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notices', function (Blueprint $table) {
            //
        });
    }
}
