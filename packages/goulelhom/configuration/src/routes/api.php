<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {

    Route::group([
        'prefix' => 'v1',
        'middleware' => 'auth:api'
    ], function () {
        Route::apiResources([
            'notices' => \Goulelhom\Configuration\App\Http\Controllers\API\NoticeController::class,
        ], [
            'except' => ['create', 'edit',]
        ]);
    });
});