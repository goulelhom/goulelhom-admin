<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 02/02/18
 * Time: 16:07
 */

namespace Goulelhom\Configuration\App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title_en
 * @property string $title_fr
 * @property string $title_ar
 * @property string $content_en
 * @property string $content_fr
 * @property string $content_ar
 * @property string $page
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Notice extends Model
{
    protected $table = 'notices';
    protected $fillable = [
        'title_en',
        'title_fr',
        'title_ar',
        'content_en',
        'content_fr',
        'content_ar',
        'page'
    ];
}