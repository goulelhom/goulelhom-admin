<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 02/02/18
 * Time: 16:27
 */

namespace Goulelhom\Configuration\App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Configuration\App\Models\Notice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class NoticeController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(Resource::collection(Notice::all()));
    }

    public function show($id)
    {
        return $this->jsonResponse(new Resource(Notice::findOrFail($id)));
    }

    public function store(Request $request)
    {
        $notice = Notice::where('page', '=', $request->get('page'))->first();
        if ($notice === null) {
            return $this->jsonResponse(new Resource(Notice::create([
                'title_en' => $request->get('title_en'),
                'title_fr' => $request->get('title_fr'),
                'title_ar' => $request->get('title_ar'),
                'content_en' => $request->get('content_en'),
                'content_fr' => $request->get('content_fr'),
                'content_ar' => $request->get('content_ar'),
                'page' => $request->get('page'),
            ])));
        } else {
            $notice->update([
                'title_en' => $request->get('title_en'),
                'title_fr' => $request->get('title_fr'),
                'title_ar' => $request->get('title_ar'),
                'content_en' => $request->get('content_en'),
                'content_fr' => $request->get('content_fr'),
                'content_ar' => $request->get('content_ar'),
            ]);
            return $this->jsonResponse($notice);
        }
    }

    public function update(Request $request, $id)
    {
        $notice = Notice::findOrFail($id);
        $notice->update([
            'title_en' => $request->get('title_en'),
            'title_fr' => $request->get('title_fr'),
            'title_ar' => $request->get('title_ar'),
            'content_en' => $request->get('content_en'),
            'content_fr' => $request->get('content_fr'),
            'content_ar' => $request->get('content_ar'),
            'page' => $request->get('page'),
        ]);
        return $this->jsonResponse(new Resource($notice));
    }

    public function destroy($id)
    {
        Notice::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}