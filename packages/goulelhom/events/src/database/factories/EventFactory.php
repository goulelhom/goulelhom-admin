<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Goulelhom\Events\App\Models\Event::class, function (Faker $faker) {
    return [
        'name_en' => $faker->realText(50),
        'description_en' => $faker->realText(191),
        'name_fr' => $faker->realText(50),
        'description_fr' => $faker->realText(191),
        'name_ar' => $faker->realText(50),
        'description_ar' => $faker->realText(191),
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'start_date' => $faker->dateTimeBetween('-1 years', '+1 years'),
        'end_date' => $faker->dateTimeBetween('-1 years', '+1 years'),
    ];
});
