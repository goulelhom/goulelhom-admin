<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');

            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();

            $table->string('name_en');
            $table->string('name_fr')->nullable();
            $table->string('name_ar')->nullable();
            $table->text('description_en');
            $table->text('description_fr')->nullable();
            $table->text('description_ar')->nullable();


            $table->decimal('longitude', 10, 7)->nullable();
            $table->decimal('latitude', 10, 7)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
