<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 20/02/18
 * Time: 03:28
 */

namespace Goulelhom\Events\App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|min:4|max:255',
            'description_en' => 'required|min:12',
        ];
    }
}