<?php

namespace Goulelhom\Events\App\Http\Controllers\Resources;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use Carbon\Carbon;
use Goulelhom\Events\App\Http\Requests\EventStoreRequest;
use Goulelhom\Events\App\Http\Resources\EventResource;
use Goulelhom\Events\App\Models\Event;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EventController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(EventResource::collection(Event::orderBy('start_date', 'asc')->get()));
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new EventResource(Event::findOrFail($id)));
    }

    public function store(EventStoreRequest $request)
    {
        return $this->jsonResponse(new EventResource(Event::create([
            'longitude' => $request->get('longitude'),
            'latitude' => $request->get('latitude'),
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'description_en' => $request->get('description_en'),
            'description_fr' => $request->get('description_fr'),
            'description_ar' => $request->get('description_ar'),
            'start_date' => new Carbon($request->get('start_date')),
            'end_date' => new Carbon($request->get('end_date')),
        ])));
    }

    public function update(EventStoreRequest $request, $id)
    {
        $record = Event::findOrFail($id);
        $record->update([
            'longitude' => $request->get('longitude'),
            'latitude' => $request->get('latitude'),
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'description_en' => $request->get('description_en'),
            'description_fr' => $request->get('description_fr'),
            'description_ar' => $request->get('description_ar'),
            'start_date' => new Carbon($request->get('start_date')),
            'end_date' => new Carbon($request->get('end_date')),
        ]);
        return $this->jsonResponse(new EventResource($record));
    }

    public function destroy($id)
    {
        Event::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function storeAttachment($id, Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000'
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new AttachmentsResource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $id,
                'attachable_type' => Event::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }

    public function destroyAttachment(Attachment $attachment)
    {
        return (string) $attachment->delete();
    }
}