<?php

namespace Goulelhom\Events\App\Models;

use App\Models\Attachment;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $fillable = [
        'start_date',
        'end_date',
        'name_en',
        'name_fr',
        'name_ar',
        'description_en',
        'description_fr',
        'description_ar',
        'longitude',
        'latitude'
    ];

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }
}