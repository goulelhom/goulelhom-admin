<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {

    Route::group([
        'prefix' => 'v1',
        'middleware' => 'auth:api'
    ], function () {
        Route::apiResources([
            'events' => \Goulelhom\Events\App\Http\Controllers\Resources\EventController::class,
        ], [
            'except' => ['create', 'edit',]
        ]);

        Route::post(
            'events/{id}/upload',
            '\Goulelhom\Events\App\Http\Controllers\Resources\EventController@storeAttachment'
        )->where('id', '[0-9]+');

        Route::delete(
            'events/{id}/upload',
            '\Goulelhom\Events\App\Http\Controllers\Resources\EventController@destroyAttachment'
        )->where('id', '[0-9]+');
    });
});