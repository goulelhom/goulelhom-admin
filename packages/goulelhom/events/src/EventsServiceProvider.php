<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 29/01/18
 * Time: 01:47
 */

namespace Goulelhom\Events;

use Goulelhom\Core\BaseServiceProvider;

class EventsServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;
}