<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Goulelhom\Claims\App\Models\Theme::class, function (Faker $faker) {
    return [
        'name_ar' => $faker->colorName,
        'name_fr' => $faker->colorName,
        'name_en' => $faker->colorName,
        'is_active' => true,
        'color' => $faker->hexColor,
    ];
});
