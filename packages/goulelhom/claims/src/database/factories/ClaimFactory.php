<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\Goulelhom\Claims\App\Models\Claim::class, function (Faker $faker) {
    return [
        'subject' => $faker->realText(50),
        'description' => $faker->realText(191),
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,

        'is_new' => mt_rand(0, 10) >= 8,
        'is_moderated' => mt_rand(0, 10) >= 8,
        'is_valid' => true,
        'has_approved_sworn_statement' => true,
        'has_approved_term_of_use' => true,

        'theme_id' => \Goulelhom\Claims\App\Models\Theme::orderByRaw("RAND()")->where('is_active', '=', true)->first()->id,
        'claimer_id' => \Goulelhom\Claims\App\Models\Claimer::orderByRaw("RAND()")->first()->id,
        'municipality_id' => \Goulelhom\Localization\App\Models\Municipality::orderByRaw("RAND()")->where('is_active', '=', true)->first()->id,
    ];
});
