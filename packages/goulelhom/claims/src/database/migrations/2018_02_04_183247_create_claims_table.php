<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');

            $table->string('subject')->nullable();
            $table->string('description');

            $table->decimal('longitude', 10, 7)->nullable();
            $table->decimal('latitude', 10, 7)->nullable();

            $table->boolean('is_new')->default(false);
            $table->boolean('is_moderated')->default(false);
            $table->boolean('is_valid')->default(false);
            $table->boolean('has_approved_sworn_statement')->default(false);
            $table->boolean('has_approved_term_of_use')->default(false);

            $table->integer('theme_id')->unsigned();
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('cascade');

            $table->integer('claimer_id')->unsigned();
            $table->foreign('claimer_id')->references('id')->on('claimers')->onDelete('cascade');

            $table->integer('municipality_id')->unsigned();
            $table->foreign('municipality_id')->references('id')->on('municipalities')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
