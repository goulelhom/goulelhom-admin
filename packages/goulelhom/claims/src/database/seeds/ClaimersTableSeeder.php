<?php

use Illuminate\Database\Seeder;

class ClaimersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Goulelhom\Claims\App\Models\Claimer::class, 100)->create();
    }
}
