<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {


    Route::group([
        'prefix' => 'v1',
    ], function () {

        Route::group([
            'prefix' => 'public',
        ], function () {

            Route::get('countries', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@countries');
            Route::get('cities', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@cities');
            Route::get('municipalities', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@municipalities');
            Route::get('themes', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@themes');
            Route::get('claims', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@claims');
            Route::get('petitions', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@petitions');
            Route::get('events', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@events');
            Route::get('reviews', '\Goulelhom\Press\App\Http\Controllers\Resources\PressReviewController@index');
            Route::get('notices', '\Goulelhom\Configuration\App\Http\Controllers\API\NoticeController@index');

            Route::get(
                'municipalities/{id}',
                '\Goulelhom\Localization\App\Http\Controllers\API\Resources\MunicipalityController@show'
            )->where('id', '[0-9]+');

            Route::get(
                'municipalities/{municipalityID}/stats',
                '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController@municipalityStat'
            )->where('municipalityID', '[0-9]+');

            Route::get(
                'municipalities/stats',
                '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController@municipalitiesStat'
            );

            Route::get(
                'events/{id}',
                '\Goulelhom\Events\App\Http\Controllers\Resources\EventController@show'
            )->where('id', '[0-9]+');



            Route::get('stats', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@stats');

            Route::post('claimers', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@saveClaimer');
            Route::post('claims', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@saveClaim');
            Route::post('claims/create', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@createClaim');
            Route::post('claims/upload/{claimID}', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\PublicController@uploadClaimFile');

        });

        Route::group([
            'middleware' => 'auth:api'
        ], function () {

            Route::get('stats/claims/counter', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController@stats');
            Route::get('stats/themes/counter', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController@statsThemes');
            Route::get('stats/municipalities/counter', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController@statsMunicipalities');

            Route::post('themes/upload', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ThemeController@storeAttachment');
            Route::delete('themes/upload', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ThemeController@destroyAttachment');


            Route::post('claims/{claimID}/upload', '\Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController@uploadClaimFile');

            Route::apiResources([
                'themes' => \Goulelhom\Claims\App\Http\Controllers\API\Resources\ThemeController::class,
                'claimers' => \Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimerController::class,
                'claims' => \Goulelhom\Claims\App\Http\Controllers\API\Resources\ClaimController::class,
                'keywords' => \Goulelhom\Claims\App\Http\Controllers\API\Resources\KeywordController::class,
            ], [
                'except' => ['create', 'edit',]
            ]);

        });
    });
});