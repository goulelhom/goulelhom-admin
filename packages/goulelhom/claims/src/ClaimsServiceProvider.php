<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 30/01/18
 * Time: 13:59
 */

namespace Goulelhom\Claims;


use Goulelhom\Core\BaseServiceProvider;

class ClaimsServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;

    function boot()
    {
        parent::boot();
    }
}