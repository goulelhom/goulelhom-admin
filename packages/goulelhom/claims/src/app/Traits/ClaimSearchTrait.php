<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 05/02/18
 * Time: 17:18
 */

namespace Goulelhom\Claims\App\Traits;


use Carbon\Carbon;

trait ClaimSearchTrait
{
    public function buildDocument()
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'coordinates' => [ 'lat' => $this->latitude , 'lon' => $this->longitude ],
            'is_new' => $this->is_new === 1,
            'is_moderated' => $this->is_moderated === 1,
            'is_valid' => $this->is_valid ===1,
            'has_approved_sworn_statement' => $this->has_approved_sworn_statement ===1,
            'has_approved_term_of_use' => $this->has_approved_term_of_use === 1,
            'theme' => $this->theme,
            'claimer' => $this->claimer,
            'municipality' => $this->municipality,
            'keywords' => $this->keywords,
        ];
    }


    public function getElasticCreatedAt ()
    {
        if ( isset( $this->created_at ) ) {
            return Carbon::createFromFormat ('Y-m-d H:i:s' , $this->created_at)->format ('Y-m-d H:i:s');
        }
    }

    public function getElasticUpdatedAt ()
    {
        if ( isset( $this->updated_at ) ) {
            return Carbon::createFromFormat ('Y-m-d H:i:s' , $this->updated_at)->format ('Y-m-d H:i:s');
        }
    }
}