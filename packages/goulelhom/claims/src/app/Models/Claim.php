<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 19:40
 */

namespace Goulelhom\Claims\App\Models;


use App\Models\Attachment;
use Goulelhom\Claims\App\Traits\ClaimSearchTrait;
use Goulelhom\Localization\App\Models\Municipality;
use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;

/**
 * @property int $id
 * @property string $subject
 * @property string $description
 * @property float $longitude
 * @property float $latitude
 * @property int $theme_id
 * @property int $claimer_id
 * @property int $municipality_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Attachment[] $attachments
 * @property Theme $theme
 * @property Claimer $claimer
 * @property Municipality $municipality
 * @property Keyword[] $keywords
 */
class Claim extends Model
{
    // TODO implement elastic search stack
    // use Searchable;
    // use ClaimSearchTrait;

    protected $table = 'claims';
    protected $fillable = [
        'subject', 'description', 'longitude', 'latitude',
        'is_new', 'is_moderated', 'is_valid', 'has_approved_sworn_statement', 'has_approved_term_of_use',
        'theme_id', 'claimer_id', 'municipality_id'];

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function theme()
    {
        return $this->belongsTo(Theme::class, 'theme_id');
    }

    public function claimer()
    {
        return $this->belongsTo(Claimer::class, 'claimer_id');
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class, 'municipality_id');
    }

    public function keywords()
    {
        return $this->belongsToMany(
            Keyword::class,
            'keywords_claims',
            'claim_id',
            'keywords_id'
            );
    }
}