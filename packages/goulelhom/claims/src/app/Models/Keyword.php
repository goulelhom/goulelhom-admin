<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 19:50
 */

namespace Goulelhom\Claims\App\Models;


use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;

/**
 * @property int $id
 * @property string $label
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Claim[] $claims
 */
class Keyword extends Model
{
    // TODO implement elastic search stack
    // use Searchable;

    protected $table = 'keywords';
    protected $fillable = ['label'];

    public function claims()
    {
        return $this->belongsToMany(
            Claim::class,
            'keywords_claims',
            'keywords_id',
            'claim_id'
        );
    }
}