<?php

namespace Goulelhom\Claims\App\Models;

use App\Models\Attachment;
use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;


/**
 * @property int $id
 * @property string $name_en
 * @property string $name_fr
 * @property string $name_ar
 * @property string $color
 * @property bool $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Attachment[] $attachments
 * @property Claim[] $claims
 */
class Theme extends Model
{
    // TODO implement elastic search stack
    // use Searchable;

    protected $table = 'themes';
    protected $fillable = ['name_en', 'name_fr', 'name_ar', 'color', 'is_active'];

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function claims()
    {
        return $this->hasMany(Claim::class);
    }
}