<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 30/01/18
 * Time: 14:07
 */

namespace Goulelhom\Claims\App\Models;


use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;

/**
 * @property int $id
 * @property string $name
 * @property string $phone_number
 * @property string $mail
 * @property string $address
 * @property string $is_banned
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property Claim[] $claims
 */
class Claimer extends Model
{
    // TODO implement elastic search stack
    // use Searchable;

    protected $table = 'claimers';
    protected $fillable = ['name', 'phone_number', 'mail', 'address', 'is_banned'];

    public function claims()
    {
        return $this->hasMany(Claim::class);
    }
}