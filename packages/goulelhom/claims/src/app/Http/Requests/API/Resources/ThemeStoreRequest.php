<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 21:58
 */

namespace Goulelhom\Claims\App\Http\Requests\API\Resources;


use Illuminate\Foundation\Http\FormRequest;

class ThemeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|min:4|max:255',
            'name_fr' => 'required|min:4|max:255',
            'name_ar' => 'required|min:4|max:255',
            'color' => 'required|min:7|max:7',
            'is_active' => 'required|boolean',
        ];
    }
}