<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 22:08
 */

namespace Goulelhom\Claims\App\Http\Requests\API\Resources;


use Illuminate\Foundation\Http\FormRequest;

class ClaimStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|min:10',
            'theme_id' => 'required|integer|exist:themes,id',
            'claimer_id' => 'required|integer|exist:claims,id',
            'municipality_id' => 'required|integer|exist:municipalities,id',
        ];
    }
}