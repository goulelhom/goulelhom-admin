<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 21:58
 */

namespace Goulelhom\Claims\App\Http\Requests\API\Resources;


use Illuminate\Foundation\Http\FormRequest;

class KeywordStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required|min:4|max:255',
        ];
    }
}