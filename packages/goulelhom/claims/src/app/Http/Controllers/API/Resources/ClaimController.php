<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 21:45
 */

namespace Goulelhom\Claims\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use Carbon\Carbon;
use Goulelhom\Claims\App\Http\Resources\ClaimResource;
use Goulelhom\Claims\App\Models\Claim;
use Goulelhom\Claims\App\Models\Keyword;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ClaimController extends Controller
{
    use RestTrait;

    public function index(Request $request)
    {
        // $records = $this->statsFilter($request);

        if (
            $request->query('start_date') &&
            $request->query('end_date')
        ) {

            $start_date = new Carbon($request->query('start_date'));
            $end_date = new Carbon($request->query('end_date'));

            if ($start_date->equalTo($end_date)) {
                $records = Claim::whereDate('claims.created_at', '=', $start_date->toDateString())
                    ->orderBy('created_at', 'desc');

            } else {
                $records = Claim::whereBetween('claims.created_at',
                    [
                        (new Carbon($start_date))->toDateString(),
                        (new Carbon($end_date))->toDateString(),
                    ]);
            }
        } else {
            $records = Claim::orderBy('created_at', 'desc');
        }

        if ($request->query('municipalities')) {
            $municipalities = explode(',', $request->query('municipalities'));
            $records = $records->whereIn('claims.municipality_id', $municipalities);
        }

        if ($request->query('themes')) {
            $themes = explode(',', $request->query('themes'));
            $records = $records->whereIn('claims.theme_id', $themes);
        }
        return $this->jsonResponse(
            ClaimResource::collection(
                $records->get()
            )
        );
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new ClaimResource(Claim::findOrFail($id)));
    }

    public function update($id, Request $request)
    {
        $keywords = $request->get('keywords');
        // var_dump($keywords);
        // die();
        // $claimKeywords = new Collection();
        $claimKeywords = [];
        foreach ($keywords as $keyword) {
            array_push($claimKeywords, Keyword::firstOrCreate(['label' => $keyword])->id);
            // $claimKeywords->push();
        }
        $params = [
            'is_new' => $request->get('is_new'),
            'is_valid' => $request->get('is_valid'),
            'is_moderated' => $request->get('is_moderated'),
        ];

        $claim = Claim::findOrFail($id);

        $claim->update($params);
        $claim->keywords()->sync($claimKeywords);

        return response()->json(new ClaimResource($claim));
    }

    public function destroy($id)
    {
        Claim::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function uploadClaimFile($claimID, Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000',
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new AttachmentsResource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $claimID,
                'attachable_type' => Claim::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }


    public function stats(Request $request)
    {
        $records = $this->statsFilter($request);
        $stat = $records
            ->select(DB::raw('COUNT(*) as count'), DB::raw('date(created_at) as dates'))
            ->groupBy('dates')
            ->orderBy('dates', 'asc')
            ->get();

        return response()->json($stat);
    }

    public function statsThemes(Request $request)
    {
        $records = $this->statsFilter($request);
        $stat = $records
            ->join('themes', 'themes.id', '=', 'theme_id')
            ->select(DB::raw('COUNT(*) as count'), DB::raw('themes.color'), DB::raw('themes.name_fr as theme'))
            ->groupBy('color')
            ->groupBy('name_fr')
            ->get();

        return response()->json($stat);
    }

    /*
    public function statsTheme(Request $request)
    {
        $records = $this->statsFilter($request);
        $stat = $records
            ->join('themes', 'themes.id', '=', 'theme_id')
            ->select(DB::raw('COUNT(*) as count'), DB::raw('themes.color'), DB::raw('themes.name_fr as theme'))
            ->groupBy('color')
            ->groupBy('name_fr')
            ->get();

        return response()->json($stat);
    }*/

    public function statsMunicipalities(Request $request)
    {
        $records = $this->statsFilter($request);
        $stat = $records
            ->join('municipalities', 'municipalities.id', '=', 'municipality_id')
            ->select(DB::raw('COUNT(*) as count'), DB::raw('municipalities.name_fr as municipality'))
            ->groupBy('municipality')
            ->get();

        return response()->json($stat);
    }

    protected function statsFilter(Request $request)
    {

        // filter by date
        if ($request->query('start_date') && $request->query('start_date') !== null) {

            $start_date = new Carbon($request->query('start_date'));
            $end_date = new Carbon($request->query('end_date'));

            if ($start_date->equalTo($end_date)) {
                $records = DB::table('claims')
                    ->whereDate('claims.created_at', '=', $start_date->toDateString());
            } else {
                $records = DB::table('claims')
                    ->whereBetween('claims.created_at',
                        [
                            (new Carbon($start_date))->toDateString(),
                            (new Carbon($end_date))->toDateString(),
                        ]);
            }
        } else {
            $records = DB::table('claims');
        }

        if ($request->query('municipalities')) {
            $municipalities = explode(',', $request->query('municipalities'));
            $records = $records->whereIn('claims.municipality_id', $municipalities);
        }

        if ($request->query('themes')) {
            $themes = explode(',', $request->query('themes'));
            $records = $records->whereIn('claims.theme_id', $themes);
        }

        return $records;
    }

    public function municipalityStat($municipalityID) {
        $stat = DB::table('claims')
            ->where('claims.municipality_id', $municipalityID)
            ->join('themes', 'themes.id', '=', 'theme_id')
            ->select(DB::raw('COUNT(*) as count'), DB::raw('themes.color'), DB::raw('themes.name_fr as theme'))
            ->groupBy('color')
            ->groupBy('name_fr')
            ->get();

        return response()->json($stat);
    }

    public function municipalitiesStat() {
        $stat = DB::table('claims')
            ->join('themes', 'themes.id', '=', 'theme_id')
            ->select(DB::raw('COUNT(*) as count'), DB::raw('themes.color'), DB::raw('themes.name_fr as theme'))
            ->groupBy('color')
            ->groupBy('name_fr')
            ->get();

        return response()->json($stat);
    }

}