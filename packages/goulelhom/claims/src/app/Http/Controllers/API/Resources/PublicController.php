<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 06/02/18
 * Time: 14:22
 */

namespace Goulelhom\Claims\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use function foo\func;
use Goulelhom\Claims\App\Http\Resources\ClaimerResource;
use Goulelhom\Claims\App\Http\Resources\ClaimResource;
use Goulelhom\Claims\App\Models\Claim;
use Goulelhom\Claims\App\Models\Claimer;
use Goulelhom\Claims\App\Models\Theme;
use Goulelhom\Events\App\Http\Resources\EventResource;
use Goulelhom\Events\App\Models\Event;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Goulelhom\Localization\App\Http\Resources\Collections\MunicipalitiesResource;
use Goulelhom\Localization\App\Http\Resources\MunicipalityResource;
use Goulelhom\Localization\App\Models\City;
use Goulelhom\Localization\App\Models\Country;
use Goulelhom\Localization\App\Models\Municipality;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PublicController extends Controller
{
    use RestTrait;

    public function themes()
    {
        return $this->jsonResponse(
            Theme::withCount('claims')
                ->orderBy('claims_count', 'desc')
                ->where('is_active', '=', true)
                ->get()
        );
    }

    public function countries()
    {
        return $this->jsonResponse(Country::all());
    }

    public function cities()
    {
        return $this->jsonResponse(City::all());
    }

    public function municipalities()
    {
        return $this->jsonResponse(
            MunicipalityResource::collection(
                Municipality::where('is_active', '=', true)->get()
            )
        );
    }

    public function claims()
    {
        return $this->jsonResponse(ClaimResource::collection(Claim::all()));
    }

    public function petitions()
    {
        return $this->jsonResponse([]);
    }

    public function events()
    {
        return $this->jsonResponse(EventResource::collection(Event::orderBy('start_date', 'asc')->get()));
    }

    public function saveClaimer(Request $request)
    {
        $claimer = Claimer::firstOrCreate(
            ['name' => $request->get('name')],
            ['phone_number' => $request->get('phone_number')]);
        return response()->json(new ClaimerResource($claimer));
    }

    public function saveClaim(Request $request)
    {
        $claim = Claim::create([
            'claimer_id' => $request->get('claimer_id'),
            'municipality_id' => $request->get('municipality_id'),
            'subject' => Theme::find($request->get('theme_id'))->name_fr,
            'theme_id' => $request->get('theme_id'),
            'description' => $request->get('description'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'is_new' => true,
        ]);
        return response()->json(new ClaimResource($claim));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createClaim(Request $request)
    {
        $claimer = Claimer::firstOrCreate(
            ['name' => $request->get('claimer_name')],
            ['phone_number' => $request->get('claimer_phone_number')]);

        $claim = Claim::create([
            'claimer_id' => $claimer->id,
            'municipality_id' => $request->get('municipality_id'),
            'subject' => Theme::find($request->get('subject'))->name_fr,
            'theme_id' => $request->get('subject'),
            'description' => $request->get('description'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'is_new' => true,
        ]);
        return response()->json(new ClaimResource($claim));
    }

    public function uploadClaimFile($claimID, Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000',
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new AttachmentsResource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $claimID,
                'attachable_type' => Claim::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }
}