<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 21:45
 */

namespace Goulelhom\Claims\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Claims\App\Http\Requests\API\Resources\KeywordStoreRequest;
use Goulelhom\Claims\App\Http\Resources\KeywordResource;
use Goulelhom\Claims\App\Models\Keyword;
use Illuminate\Http\JsonResponse;

class KeywordController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(KeywordResource::collection(Keyword::all()));
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new KeywordResource(Keyword::findOrFail($id)));
    }

    public function store(KeywordStoreRequest $request)
    {
        return $this->jsonResponse(new KeywordResource(Keyword::create([
            'label' => $request->get('label'),
            'is_active' => $request->get('is_active'),
        ])));
    }

    public function update(KeywordStoreRequest $request, $id)
    {
        $theme = Keyword::findOrFail($id);
        $theme->update([
            'label' => $request->get('label'),
            'is_active' => $request->get('is_active'),
        ]);
        return $this->jsonResponse(new KeywordResource($theme));
    }

    public function destroy($id)
    {
        Keyword::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}