<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 30/01/18
 * Time: 14:08
 */

namespace Goulelhom\Claims\App\Http\Controllers\API\Resources;

use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Claims\App\Http\Resources\ClaimerResource;
use Goulelhom\Claims\App\Models\Claimer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ClaimerController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(ClaimerResource::collection(
            Claimer::orderBy('created_at', 'desc')->get())
        );
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new ClaimerResource(Claimer::findOrFail($id)));
    }

    public function store(Request $request)
    {
        $claimer = Claimer::create([
            'name' => $request->get('name'),
            'phone_number' => $request->get('phone_number'),
            'mail' => $request->get('mail'),
            'address' => $request->get('address'),
        ]);
        return response()->json(new ClaimerResource($claimer));
    }

    public function update($id, Request $request)
    {
        $claimer = Claimer::findOrFail($id);
        $claimer->update([
            'name' => $request->get('name'),
            'phone_number' => $request->get('phone_number'),
            'mail' => $request->get('mail'),
            'address' => $request->get('address'),
        ]);
        return response()->json(new ClaimerResource($claimer));
    }

    public function destroy($id)
    {
        Claimer::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}