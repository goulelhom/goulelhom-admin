<?php

namespace Goulelhom\Claims\App\Http\Controllers\API\Resources;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Claims\App\Http\Requests\API\Resources\ThemeStoreRequest;
use Goulelhom\Claims\App\Http\Resources\ThemeResource;
use Goulelhom\Claims\App\Models\Theme;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class ThemeController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(ThemeResource::collection(Theme::all()));
    }

    public function show(int $id)
    {
        return $this->jsonResponse(new ThemeResource(Theme::findOrFail($id)));
    }

    public function store(ThemeStoreRequest $request)
    {
        return $this->jsonResponse(new ThemeResource(Theme::create([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'color' => $request->get('color'),
            'is_active' => $request->get('is_active'),
        ])));
    }

    public function update(ThemeStoreRequest $request, $id)
    {
        $theme = Theme::findOrFail($id);
        $theme->update([
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
            'color' => $request->get('color'),
            'is_active' => $request->get('is_active'),
        ]);
        return $this->jsonResponse(new ThemeResource($theme));
    }

    public function destroy($id)
    {
        Theme::findOrFail($id)
            ->delete();

        return $this->jsonResponse(
            'record deleted successfully',
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    public function storeAttachment(Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000',
            'attachable_id' => 'required|integer'
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new Resource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $request->get('attachable_id'),
                'attachable_type' => Theme::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }

    public function destroyAttachment(Attachment $attachment)
    {
        return (string) $attachment->delete();
    }

    public function stats()
    {
        return $this->jsonResponse(
            Theme::withCount('claims')
                ->get()
        );
    }
}