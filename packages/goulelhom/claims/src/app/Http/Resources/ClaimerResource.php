<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 22:09
 */

namespace Goulelhom\Claims\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class ClaimerResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone_number' => $this->phone_number,
            'mail' => $this->mail,
            'address' => $this->address,
            'is_banned' => $this->is_banned === 1,
            'claims' => $this->claims->sortByDesc('created_at'),
            'claims_count' => $this->claims->count(),
            'created_at' => (string) $this->created_at,
        ];
    }
}