<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 22:09
 */

namespace Goulelhom\Claims\App\Http\Resources;


use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsLightboxResource;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Illuminate\Http\Resources\Json\Resource;

class ClaimPublicResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'attachments' => AttachmentsLightboxResource::collection($this->attachments),
            'theme' => $this->theme,
            'claimer' => $this->claimer,
            'municipality' => $this->municipality,
            'keywords' => KeywordResource::collection($this->keywords),
            'cover' => new AttachmentsResource($this->attachments()->first()),
            'created_at' => (string) $this->created_at
        ];
    }
}