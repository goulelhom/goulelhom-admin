<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 21:46
 */

namespace Goulelhom\Claims\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class ThemeResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
            'color' => $this->color,
            'is_active' => $this->is_active === 1,
            'attachments' => $this->attachments,
            'claims' => $this->claims,
        ];
    }
}