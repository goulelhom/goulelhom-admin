<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 22/02/18
 * Time: 04:46
 */

namespace Goulelhom\Claims\App\Http\Resources;


use Illuminate\Http\Resources\Json\ResourceCollection;

class PaginateClaimResource extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => ClaimResource::collection($this->collection),
            'pagination' => [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ],
        ];
    }

    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        unset($originalContent['links'],$originalContent['meta']);
        $response->setData($originalContent);
    }
}