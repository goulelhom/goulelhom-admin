<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 22:09
 */

namespace Goulelhom\Claims\App\Http\Resources;


use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Illuminate\Http\Resources\Json\Resource;

class ClaimResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'is_new' => $this->is_new === 1,
            'is_moderated' => $this->is_moderated === 1,
            'is_valid' => $this->is_valid === 1,
            'has_approved_sworn_statement' => $this->has_approved_sworn_statement === 1,
            'has_approved_term_of_use' => $this->has_approved_term_of_use === 1,
            'attachments' => AttachmentsResource::collection($this->attachments),
            'theme' => $this->theme,
            'claimer' => $this->claimer,
            'municipality' => $this->municipality,
            'keywords' => KeywordResource::collection($this->keywords),
            'cover' => new AttachmentsResource($this->attachments()->first()),
        ];
    }
}