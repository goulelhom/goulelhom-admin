<?php
/**
 * Created by PhpStorm.
 * User: chams
 * Date: 04/02/18
 * Time: 21:46
 */

namespace Goulelhom\Claims\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class KeywordResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            // 'claims' => $this->claims,
        ];
    }
}