<?php
namespace Goulelhom\Resources;

use Goulelhom\Core\BaseServiceProvider;

/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 04:53
 */

class ResourcesServiceProvider extends BaseServiceProvider
{
    protected $dir = __DIR__;
}