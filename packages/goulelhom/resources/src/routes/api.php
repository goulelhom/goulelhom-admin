<?php

use \Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
    'prefix' => 'api',
], function () {


    Route::group([
        'prefix' => 'v1',
    ], function () {

        Route::group([
            'prefix' => 'public',
        ], function () {

            Route::apiResources([
                'petitioners' => \Goulelhom\Resources\App\Http\Controllers\PetitionerController::class,
                'targets' => \Goulelhom\Resources\App\Http\Controllers\TargetController::class,
                'petitions' => \Goulelhom\Resources\App\Http\Controllers\PetitionController::class,
                'signatures' => \Goulelhom\Resources\App\Http\Controllers\SignatureController::class,
            ], [
                'except' => ['create', 'edit', 'update', 'delete']
            ]);
            Route::post('contact', '\Goulelhom\Resources\App\Http\Controllers\ContactFormController@sendMail');
            Route::post('petitions/upload/{petitionsID}', '\Goulelhom\Resources\App\Http\Controllers\PetitionController@upload');
        });
    });
});