<?php

namespace Goulelhom\Resources\App\Http\Resources;


use Goulelhom\Claims\App\Http\Resources\ThemeResource;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Goulelhom\Petitions\App\Models\Target;
use Illuminate\Http\Resources\Json\Resource;

class PetitionResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start_date' => (string) $this->start_date,
            'end_date' => (string) $this->end_date,

            'theme' => new ThemeResource($this->theme),
            'petitioner' => new PetitionerResource($this->petitioner),
            'target' => new TargetResource($this->target),

            'name' => $this->name,
            'description' => $this->description,
            'requested_signatures_number' => $this->requested_signatures_number,
            'status' => $this->status,
            'is_boosted' => $this->is_boosted == 1,
            'uuid' => $this->uuid,
            'signatures' => $this->signatures->count(),
            'cover' => new AttachmentsResource($this->attachments->first()),
            'attachments' => AttachmentsResource::collection($this->attachments),
            'haveReachedSignaturesNumber' => $this->haveReachedSignaturesNumber(),
        ];
    }
}