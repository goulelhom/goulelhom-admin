<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 28/02/18
 * Time: 04:11
 */

namespace Goulelhom\Resources\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class PetitionerResource extends Resource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name
        ];
    }
}