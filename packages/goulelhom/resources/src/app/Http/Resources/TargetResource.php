<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 20:52
 */

namespace Goulelhom\Resources\App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class TargetResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fr' => $this->name_fr,
            'name_ar' => $this->name_ar,
        ];
    }
}