<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 05:20
 */

namespace Goulelhom\Resources\App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PetitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'theme_id' => 'required|integer|exists:themes,id',
            'petitioner_id' => 'required|integer|exists:themes,id',
            'target_id' => 'required|integer',
            'name' => 'required|string|min:4|max:150',
            'description' => 'required|string|min:4',
            'requested_signatures_number' => 'required|numeric|min:0',
        ];
    }
}