<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 05:20
 */

namespace Goulelhom\Resources\App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PetitionerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:150',
            'mail' => 'nullable|min:4|max:255|e-mail',
            'phone_number' => 'nullable|min:8|max:20',
            'address' => 'nullable|min:4|max:255',
        ];
    }
}