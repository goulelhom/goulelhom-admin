<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 05:20
 */

namespace Goulelhom\Resources\App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:4|max:150',
            'email' => 'required|email',
            'phone_number' => 'required|string|min:8|max:20',
            'subject' => 'required|string|min:4|max:150',
            'message' => 'required|string|min:10',
        ];
    }
}