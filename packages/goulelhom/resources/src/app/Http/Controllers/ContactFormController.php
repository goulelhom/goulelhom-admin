<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 28/02/18
 * Time: 10:15
 */

namespace Goulelhom\Resources\App\Http\Controllers;


use App\Mail\ContactForm;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Resources\App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;

class ContactFormController
{
    use RestTrait;

    public function sendMail(ContactRequest $request)
    {
        $contact = [
            'name' => $request->get('name'),
            'phone_number' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'message' => $request->get('message'),
        ];

        $to = env('MAIL_TO');

        Mail::to($to)->send(new ContactForm($contact));

        return $this->jsonResponse($contact);
    }
}