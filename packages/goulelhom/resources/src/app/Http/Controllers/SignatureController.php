<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 28/02/18
 * Time: 04:41
 */

namespace Goulelhom\Resources\App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Petitions\App\Models\Signature;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class SignatureController extends Controller
{
    use RestTrait;

    public function store(Request $request) {
        $params = [
            'petitioner_id' => $request->get('petitioner_id'),
            'petition_id' => $request->get('petition_id'),
        ];

        $record = Signature::firstOrCreate([
            'petitioner_id' => $params['petitioner_id'],
            'petition_id' => $params['petition_id']
        ]);

        return $this->jsonResponse(new Resource($record), JsonResponse::HTTP_CREATED);
    }
}