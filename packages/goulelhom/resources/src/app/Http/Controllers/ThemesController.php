<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 20:50
 */

namespace Goulelhom\Resources\App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;

class ThemesController extends Controller
{
    use RestTrait;
}