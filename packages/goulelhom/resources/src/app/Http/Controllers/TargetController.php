<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 20:51
 */

namespace Goulelhom\Resources\App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Petitions\App\Models\Target;
use Goulelhom\Resources\App\Http\Resources\TargetResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TargetController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(TargetResource::collection(Target::all()));
    }

    public function store(Request $request)
    {
        $params = [
            'name_en' => $request->get('name_en'),
            'name_fr' => $request->get('name_fr'),
            'name_ar' => $request->get('name_ar'),
        ];

        if ($params['name_en'] !== null) {
            return $this->jsonResponse(
                new TargetResource(Target::firstOrCreate($params)),
                JsonResponse::HTTP_CREATED
            );
        }

        if ($params['name_fr'] !== null) {
            return $this->jsonResponse(
                new TargetResource(Target::firstOrCreate($params)),
                JsonResponse::HTTP_CREATED
            );
        }

        if ($params['name_ar'] !== null) {
            return $this->jsonResponse(
                new TargetResource(Target::firstOrCreate($params)),
                JsonResponse::HTTP_CREATED
            );
        }

        return $this->jsonResponse(
            'the name is required...',
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        );
    }

}