<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 05:14
 */

namespace Goulelhom\Resources\App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Traits\API\Helpers\RestTrait;
use Goulelhom\Petitions\App\Models\Petitioner;
use Goulelhom\Resources\App\Http\Requests\PetitionerRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\Resource;

class PetitionerController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(
            Resource::collection(Petitioner::all())
        );
    }

    public function store(PetitionerRequest $request)
    {
        $params = [
            'name' => $request->get('name'),
            'mail' => $request->get('mail'),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
        ];

        $petitioner = Petitioner::firstOrNew(['name' => $params['name'], 'phone_number' => $params['phone_number']]);

        $petitioner->mail = $params['mail'];
        $petitioner->address = $params['address'];

        $petitioner->save();

        return $this->jsonResponse(
            new Resource($petitioner),
            JsonResponse::HTTP_CREATED
        );
    }
}