<?php
/**
 * Created by PhpStorm.
 * User: mouedhen
 * Date: 27/02/18
 * Time: 05:13
 */

namespace Goulelhom\Resources\App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Traits\API\Helpers\RestTrait;
use Carbon\Carbon;
use Goulelhom\Localization\App\Http\Resources\Collections\AttachmentsResource;
use Goulelhom\Petitions\App\Models\Petition;
use Goulelhom\Resources\App\Http\Requests\PetitionerRequest;
use Goulelhom\Resources\App\Http\Resources\PetitionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class PetitionController extends Controller
{
    use RestTrait;

    public function index()
    {
        return $this->jsonResponse(PetitionResource::collection(Petition::all()));
    }

    public function show($uuid)
    {
        $record = Petition::where('uuid', '=', $uuid)->firstOrFail();
        return $this->jsonResponse(new PetitionResource($record));
    }

    public function store(Request $request)
    {
        $params = [
            'start_date' => (string) new Carbon(),
            'end_date' => new Carbon($request->get('end_date')),
            'petitioner_id' => $request->get('petitioner_id'),
            'theme_id' => $request->get('theme_id'),
            'target_id' => $request->get('target_id'),
            'requested_signatures_number' => $request->get('requested_signatures_number'),
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'uuid' => (string) Uuid::generate(4),
        ];

        $petition = Petition::create($params);

        return $this->jsonResponse($petition, JsonResponse::HTTP_CREATED);
    }

    public function upload($petitionID, Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:5000',
        ]);

        if ($fileUid = $request->file->store('/upload', 'public')) {
            return $this->jsonResponse(new AttachmentsResource(Attachment::create([
                'filename' => $request->file->getClientOriginalName(),
                'uid' => $fileUid,
                'size' => $request->file->getClientSize(),
                'mime' => $request->file->getMimeType(),
                'attachable_id' => $petitionID,
                'attachable_type' => Petition::class,
            ])));
        }
        return $this->jsonResponse('Unable to upload your file.', JsonResponse::HTTP_BAD_REQUEST);
    }
}