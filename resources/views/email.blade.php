@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h1 class="primary"><strong>[GOULEL'HOM] {{ $contact['subject'] }}</strong></h1>

    <div style="text-align: right">
        <dl>
            <dt><strong>Sender name </strong></dt><dd>{{ $contact['name'] }}</dd>
            <dt><strong>Sender E-mail </strong></dt><dd>{{ $contact['email'] }}</dd>
        </dl>
    </div>
    <h2 class="secondary"><strong>{{ $contact['subject'] }}</strong></h2>
    <p>{{ $contact['message'] }}</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop