const apiDomain = Laravel.apiDomain + '/v1';
const siteName = Laravel.siteName;

export {apiDomain, siteName};
