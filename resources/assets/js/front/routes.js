import HomePage from './pages/HomePage'
import ClaimsPage from './pages/ClaimsPage'
import PetitionsPage from './pages/PetitionsPage'
import PetitionsDetailsPage from './pages/PetitionsDetailsPage'
import EventsPage from './pages/EventsPage'
import MunicipalitiesPage from './pages/MunicipalitiesPage'
import MunicipalityDetailsPage from './pages/MunicipalityDetailsPage'
import ReportsPage from './pages/ReportsPage'
import PressPage from './pages/PressPage'

import ConfidentialityAndCookiesPage from './pages/ConfidentialityAndCookiesPage'
import LegalNoticePage from './pages/LegalNoticePage'
import SwormStatementPage from './pages/SwormStatementPage'
import TermOfUsePage from './pages/TermOfUsePage'

import ContactUsPage from './pages/ContactUsPage'

import UnderConstructionPage from './pages/UnderConstructionPage'

export const routes = [
    {
        path: '/',
        name: 'public:index',
        components: {
            content: HomePage
        },
    },
    {
        path: '/complains',
        name: 'public:claims',
        components: {
            content: ClaimsPage
        },
    },
    {
        path: '/petitions',
        name: 'public:petitions',
        components: {
            content: PetitionsPage
        },
    },
    {
        path: '/petitions/:uuid',
        name: 'public:petitions:details',
        components: {
            content: PetitionsDetailsPage
        },
    },
    {
        path: '/events',
        name: 'public:events',
        components: {
            content: EventsPage
        },
    },
    {
        path: '/municipalities',
        name: 'public:municipalities',
        components: {
            content: MunicipalitiesPage
        },
    },
    {
        path: '/municipalities/:id/details',
        name: 'public:municipalities:details',
        components: {
            content: MunicipalityDetailsPage
        },
    },
    {
        path: '/reports',
        name: 'public:reports',
        components: {
            content: UnderConstructionPage
        },
    },
    {
        path: '/press',
        name: 'public:press',
        components: {
            content: PressPage
        },
    },
    {
        path: '/confidentiality-and-cookies',
        name: 'pages:confidentiality_and_cookies',
        components: {
            content: ConfidentialityAndCookiesPage
        },
    },
    {
        path: '/legal-notice',
        name: 'pages:legal_notice',
        components: {
            content: LegalNoticePage
        },
    },
    {
        path: '/sworn-statement',
        name: 'pages:sworn_statement',
        components: {
            content: SwormStatementPage
        },
    },
    {
        path: '/term-of-use',
        name: 'pages:term_of_use',
        components: {
            content: TermOfUsePage
        },
    },
    {
        path: '/contact-us',
        name: 'pages:contact-us',
        components: {
            content: ContactUsPage
        },
    },
];
