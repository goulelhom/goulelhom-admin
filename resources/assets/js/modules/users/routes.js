import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import UsersIndex from './users/pages/UsersIndex'
import UsersCreate from './users/pages/UsersCreate'
import UsersList from './users/pages/UsersList'
import UsersDetails from './users/pages/UsersDetails'

export const routes = [
    {
        path: '/dashboard/users',
        name: 'dashboard:users:index',
        components: {
            navigation: NavBar,
            content: UsersIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/users/create',
        name: 'dashboard:users:create',
        components: {
            navigation: NavBar,
            content: UsersCreate
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/users/list',
        name: 'dashboard:users:list',
        components: {
            navigation: NavBar,
            content: UsersList
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/users/:id/details',
        name: 'dashboard:users:details',
        components: {
            navigation: NavBar,
            content: UsersDetails
        },
        beforeEnter: authMiddleware,
    }
];
