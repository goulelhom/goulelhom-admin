import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import ConfigurationIndex from './shared/pages/ConfigurationIndex'
import NoticeIndex from './notices/pages/NoticeIndex'
import NoticesDetails from './notices/pages/NoticesDetails'

export const routes = [
    {
        path: '/dashboard/configuration',
        name: 'dashboard:configuration:index',
        components: {
            navigation: NavBar,
            content: ConfigurationIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/configuration/notices',
        name: 'dashboard:configuration:notices',
        components: {
            navigation: NavBar,
            content: NoticeIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/configuration/notices/:id/:page/:lang',
        name: 'dashboard:configuration:notices:details',
        components: {
            navigation: NavBar,
            content: NoticesDetails
        },
        beforeEnter: authMiddleware,
    },
];
