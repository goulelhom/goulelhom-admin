import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import PressIndex from './pages/PressIndex'
import PressReviewCreate from './pages/PressReviewCreate'
import PressReviewList from './pages/PressReviewList'

export const routes = [
    {
        path: '/dashboard/press',
        name: 'dashboard:press:index',
        components: {
            navigation: NavBar,
            content: PressIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/press/create',
        name: 'dashboard:press:create',
        components: {
            navigation: NavBar,
            content: PressReviewCreate
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/press/list',
        name: 'dashboard:press:list',
        components: {
            navigation: NavBar,
            content: PressReviewList
        },
        beforeEnter: authMiddleware,
    },
];
