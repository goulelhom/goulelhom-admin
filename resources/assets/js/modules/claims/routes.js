import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import ClaimsModuleIndex from './shared/pages/ClaimsModuleIndex'
import ClaimersIndex from './claimers/pages/ClaimersIndex'
import ClaimsIndex from './claims/pages/ClaimsIndex'
import ClaimsDetails from './claims/pages/ClaimsDetails'
import ThemesIndex from './themes/pages/ThemesIndex'

export const routes = [
    {
        path: '/dashboard/claims',
        name: 'dashboard:claims:index',
        components: {
            navigation: NavBar,
            content: ClaimsModuleIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/claims/claimers',
        name: 'dashboard:claims:claimers:index',
        components: {
            navigation: NavBar,
            content: ClaimersIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/claims/claims',
        name: 'dashboard:claims:claims:index',
        components: {
            navigation: NavBar,
            content: ClaimsIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/claims/claims/:id/details',
        name: 'dashboard:claims:claims:details',
        components: {
            navigation: NavBar,
            content: ClaimsDetails
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/claims/themes',
        name: 'dashboard:claims:themes:index',
        components: {
            navigation: NavBar,
            content: ThemesIndex
        },
        beforeEnter: authMiddleware,
    },
];
