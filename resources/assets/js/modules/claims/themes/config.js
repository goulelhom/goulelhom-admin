export function initialThemesDetails() {
    return {
        id: -1,
        name_en: null,
        name_fr: null,
        name_ar: null,
        color: null,
        is_active : false,
        attachments : [],
        claims : [],
    }
}

export const initialValidationError = function () {
    return {
        id: -1,
        name_en: '',
        name_fr: '',
        name_ar: '',
    }
};
