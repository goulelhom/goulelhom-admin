export function claimInitialData() {
    return {
        id: -1,
        subject: null,
        description: null,
        latitude: null,
        longitude: null,
        is_new: false,
        is_valid: false,
        is_moderated: false,
        has_approved_sworn_statement: null,
        has_approved_term_of_use: null,
        keywords: [],
        attachments: [],
        theme: {
            id: -1,
            name_fr: null,
        },
        municipality: {
            id: -1,
            name_fr: null,
        },
        claimer: {
            id: -1,
            name: null,
            mail: null,
            phone_number: null,
            is_banned: null,
            address: null,
        },
    }
}