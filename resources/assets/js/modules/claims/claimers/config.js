export function initialClaimerDetails() {
    return {
        id: -1,
        name: null,
        phone_number: null,
        mail: null,
        address: null,
        is_banned: false,
        claims: [],
        created_at: null,
    }
}