export function initialMunicipalitiesDetails() {
    return {
        id: -1,
        name_en: '',
        name_fr: '',
        name_ar: '',

        population: null,
        houses: null,
        regional_council_number: null,
        municipal_council_number: null,

        website: '',
        phone: '',
        email: '',
        fax: '',

        description_en: '',
        description_fr: '',
        description_ar: '',

        is_active: false,
        city_id: null,

        city: {},
        attachments: [],
    }
}

export const initialValidationError = function () {
    return {
        id: -1,
        name_en: '',
        name_fr: '',
        name_ar: '',
        city_id: ''
    }
};
