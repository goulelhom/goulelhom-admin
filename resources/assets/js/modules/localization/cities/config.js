export function initialCitiesDetails() {
    return {
        id: -1,
        name_en: null,
        name_fr: null,
        name_ar: null,
        country_id: null,
        population: 0,
    }
}

export const initialValidationError = function () {
    return {
        id: -1,
        name_en: '',
        name_fr: '',
        name_ar: '',
        country_id: ''
    }
};
