export function initialCountriesDetails() {
    return {
        id: -1,
        name_en: null,
        name_fr: null,
        name_ar: null,
    }
}

export const initialValidationError = function () {
    return {
        id: -1,
        name_en: '',
        name_fr: '',
        name_ar: '',
    }
};

