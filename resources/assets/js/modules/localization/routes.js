import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import LocalizationIndex from './shared/pages/LocalizationIndex'
import CountriesIndex from './countries/pages/CountriesIndex'
import CitiesIndex from './cities/pages/CitiesIndex'

import MunicipalitiesIndex from './municipalities/pages/MunicipalitiesIndex'
import MunicipalitiesCreate from './municipalities/pages/MunicipalitiesCreate'
import MunicipalitiesList from './municipalities/pages/MunicipalitiesList'
import MunicipalitiesEdit from './municipalities/pages/MunicipalitiesEdit'
import MunicipalitiesShow from './municipalities/pages/MunicipalitiesShow'

export const routes = [
    {
        path: '/dashboard/localization',
        name: 'dashboard:localization:index',
        components: {
            navigation: NavBar,
            content: LocalizationIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/config/countries',
        name: 'dashboard:localization:countries',
        components: {
            navigation: NavBar,
            content: CountriesIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/config/cities',
        name: 'dashboard:localization:cities',
        components: {
            navigation: NavBar,
            content: CitiesIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/municipalities',
        name: 'dashboard:localization:municipalities',
        components: {
            navigation: NavBar,
            content: MunicipalitiesIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/municipalities/create',
        name: 'dashboard:localization:municipalities:create',
        components: {
            navigation: NavBar,
            content: MunicipalitiesCreate
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/municipalities/list',
        name: 'dashboard:localization:municipalities:list',
        components: {
            navigation: NavBar,
            content: MunicipalitiesList
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/municipalities/:id/edit',
        name: 'dashboard:localization:municipalities:edit',
        components: {
            navigation: NavBar,
            content: MunicipalitiesEdit
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/localization/municipalities/:id/details',
        name: 'dashboard:localization:municipalities:details',
        components: {
            navigation: NavBar,
            content: MunicipalitiesShow
        },
        beforeEnter: authMiddleware,
    },
];
