import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import PetitonsList from './petitions/PetitionsList'

export const routes = [
    {
        path: '/dashboard/petitions',
        name: 'dashboard:petitions:index',
        components: {
            navigation: NavBar,
            content: PetitonsList
        },
        beforeEnter: authMiddleware,
    },
];
