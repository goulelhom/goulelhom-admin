import authMiddleware from './../../routes/middleware/auth'
import NavBar from './../../components/shared/NavBar'

import EventsIndex from './events/pages/EventsIndex'
import EventsList from './events/pages/EventsList'
import EventsCreate from './events/pages/EventsCreate'

export const routes = [
    {
        path: '/dashboard/events',
        name: 'dashboard:events:index',
        components: {
            navigation: NavBar,
            content: EventsIndex
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/events/create',
        name: 'dashboard:events:create',
        components: {
            navigation: NavBar,
            content: EventsCreate
        },
        beforeEnter: authMiddleware,
    },
    {
        path: '/dashboard/events/list',
        name: 'dashboard:events:list',
        components: {
            navigation: NavBar,
            content: EventsList
        },
        beforeEnter: authMiddleware,
    },
];
