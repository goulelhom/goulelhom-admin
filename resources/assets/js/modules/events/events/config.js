export function eventInitialData() {
    return {
        id: -1,
        longitude: null,
        latitude: null,
        name_en: null,
        name_fr: null,
        name_ar: null,
        description_en: null,
        description_fr: null,
        description_ar: null,
        start_date: null,
        end_date: null,
    }
}