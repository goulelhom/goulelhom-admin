import VueRouter from 'vue-router'

import NavBar from './../components/shared/NavBar'
import Home from './../components/pages/Home'
import Login from './../components/pages/auth/Login'

import {routes as publicRoutes} from './../front/routes'

import {routes as usersRoutes} from './../modules/users/routes'
import {routes as localizationRoutes} from './../modules/localization/routes'
import {routes as claimsRoutes} from './../modules/claims/routes'
import {routes as eventsRoutes} from './../modules/events/routes'
import {routes as pressRoutes} from './../modules/press/routes'

import {routes as configurationRoutes} from './../modules/configuration/routes'

import {routes as petitionsRoutes} from './../modules/petitions/routes'

const router = new VueRouter({
    mode: 'history',
    routes: [
        ...publicRoutes,
        {
            path: '/dashboard',
            name: 'index',
            components: {
                navigation: NavBar,
                content: Home
            },
        },
        {
            path: '/login',
            name: 'auth:login',
            components: {
                navigation: NavBar,
                content: Login
            },

        },
        ...usersRoutes,
        ...localizationRoutes,
        ...claimsRoutes,
        ...eventsRoutes,
        ...pressRoutes,

        ...petitionsRoutes,

        ...configurationRoutes,
    ]
});

export default router;
