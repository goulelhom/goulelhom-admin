import 'babel-polyfill'
import axios from 'axios'
import * as Cookies from 'tiny-cookie'

window._ = require('lodash');

axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
    // 'Access-Control-Allow-Origin': '*',
    'Authorization': Cookies.get('token'),
};

window.Cookie = Cookies;
window.axios = axios;

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

Vue.use(VueRouter);
Vue.use(Vuex);

import ElementUI from 'element-ui'
import DataTables from 'vue-data-tables'
import locale from 'element-ui/lib/locale/lang/ar'
import VueQuillEditor from 'vue-quill-editor'

Vue.use(ElementUI, { locale });
Vue.use(DataTables);
Vue.use(VueQuillEditor, /* { default global options } */);

import {VueMasonryPlugin} from 'vue-masonry';

Vue.use(VueMasonryPlugin);

let SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

window.Vue = Vue;

import App from './components/App.vue'
import router from './routes'
import store from './store'

import VueI18n from 'vue-i18n'
Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: 'fr',
    fallbackLocale: 'en',
    messages: {
        'en': require('./locales/en.json'),
        'fr': require('./locales/fr.json'),
        'ar': require('./locales/ar.json'),
    }
});

import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
    id: 'UA-67496136-2',
    router
});

Vue.component('app', App);
const app = new Vue({
    router,
    store,
    i18n,
}).$mount('#app');

