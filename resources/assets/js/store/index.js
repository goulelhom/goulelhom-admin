import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import auth from './modules/auth'
import users from './modules/users/users'
import config from './modules/config'

import countries from './modules/localization/countries'
import cities from './modules/localization/cities'
import municipalities from './modules/localization/municipalities'
import events from './modules/events/events'
import reviews from './modules/press/review'

import notices from './modules/configuration/notices'

import claimers from './modules/claims/claimers'
import claims from './modules/claims/claims'
import keywords from './modules/claims/keywords'
import themes from './modules/claims/themes'

import petitions from './modules/petitions/petitions'

export default new Vuex.Store({
    modules: {
        auth,
        users,
        config,

        countries,
        cities,
        municipalities,
        events,
        reviews,

        notices,

        claimers,
        claims,
        keywords,
        themes,

        petitions,
    },
    strict: false,
});
