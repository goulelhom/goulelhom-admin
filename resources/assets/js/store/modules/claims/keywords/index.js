import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_KEYWORDS,
    FETCH_KEYWORDS,
    FETCH_KEYWORD,
    CREATE_KEYWORD,
    UPDATE_KEYWORD,
    DELETE_KEYWORD
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_KEYWORDS] (state, keywords) {
        state.all = initialState.all
    },

    [FETCH_KEYWORDS] (state, keywords) {
        state.all = keywords
    },

    [FETCH_KEYWORD] (state, keyword) {
        const index = state.all.findIndex(x => x.id === keyword.id);
        if (index === -1) {
            state.all.push(keyword)
        } else {
            state.all.splice(index, 1, keyword)
        }
    },

    [CREATE_KEYWORD] (state, keyword) {
        state.all.push(keyword)
    },

    [UPDATE_KEYWORD] (state, keyword) {
        const index = state.all.findIndex(x => x.id === keyword.id);
        if (index !== -1) {
            state.all.splice(index, 1, keyword)
        }
    },

    [DELETE_KEYWORD] (state, keywordID) {
        state.all = state.all.filter(x => x.id !== keywordID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
