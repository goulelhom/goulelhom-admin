import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_KEYWORDS,
    FETCH_KEYWORDS,
    FETCH_KEYWORD,
    CREATE_KEYWORD,
    UPDATE_KEYWORD,
    DELETE_KEYWORD
} from './mutation-types'

export async function reinitKeywords({commit}) {
    commit(REINIT_KEYWORDS, [])
}

export async function fetchKeywords({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/keywords',
        })
        .then(response => {
            commit(FETCH_KEYWORDS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchKeyword({commit}, {keywordID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/keywords/' + keywordID,
        })
        .then(response => {
            commit(FETCH_KEYWORD, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createKeyword({commit}, {keyword}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/keywords',
            data: keyword
        })
        .then(response => {
            commit(CREATE_KEYWORD, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateKeyword({commit}, {keyword}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/keywords/' + keyword.id,
            data: keyword
        })
        .then(response => {
            commit(UPDATE_KEYWORD, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteKeyword({commit}, { keywordID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/keywords/' + keywordID
        })
        .then(response => {
            commit(DELETE_KEYWORD, keywordID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveKeyword({commit, state}, { keyword }) {
    const index = state.all.findIndex((x) => x.id === keyword.id);
    if (index !== -1 ) {
        return updateKeyword({commit}, {keyword})
    }
    return createKeyword({commit}, {keyword})
}
