export const REINIT_KEYWORDS = 'claims/keywords/REINIT_KEYWORDS';
export const FETCH_KEYWORDS = 'claims/keywords/FETCH_KEYWORDS';
export const FETCH_KEYWORD = 'claims/keywords/FETCH_KEYWORD';
export const CREATE_KEYWORD = 'claims/keywords/CREATE_KEYWORD';
export const UPDATE_KEYWORD = 'claims/keywords/UPDATE_KEYWORD';
export const DELETE_KEYWORD = 'claims/keywords/DELETE_KEYWORD';
