import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_CLAIMS,
    FETCH_CLAIMS,
    FETCH_CLAIM,
    CREATE_CLAIM,
    UPDATE_CLAIM,
    DELETE_CLAIM
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_CLAIMS] (state, claims) {
        state.all = initialState.all
    },

    [FETCH_CLAIMS] (state, claims) {
        state.all = claims
    },

    [FETCH_CLAIM] (state, claim) {
        const index = state.all.findIndex(x => x.id === claim.id);
        if (index === -1) {
            state.all.push(claim)
        } else {
            state.all.splice(index, 1, claim)
        }
    },

    [CREATE_CLAIM] (state, claim) {
        state.all.push(claim)
    },

    [UPDATE_CLAIM] (state, claim) {
        const index = state.all.findIndex(x => x.id === claim.id);
        if (index !== -1) {
            state.all.splice(index, 1, claim)
        }
    },

    [DELETE_CLAIM] (state, claimID) {
        state.all = state.all.filter(x => x.id !== claimID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
