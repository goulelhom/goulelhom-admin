export const REINIT_CLAIMS = 'claims/claims/REINIT_CLAIMS';
export const FETCH_CLAIMS = 'claims/claims/FETCH_CLAIMS';
export const FETCH_CLAIM = 'claims/claims/FETCH_CLAIM';
export const CREATE_CLAIM = 'claims/claims/CREATE_CLAIM';
export const UPDATE_CLAIM = 'claims/claims/UPDATE_CLAIM';
export const DELETE_CLAIM = 'claims/claims/DELETE_CLAIM';
