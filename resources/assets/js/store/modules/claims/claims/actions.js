import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_CLAIMS,
    FETCH_CLAIMS,
    FETCH_CLAIM,
    CREATE_CLAIM,
    UPDATE_CLAIM,
    DELETE_CLAIM
} from './mutation-types'

export async function reinitClaims({commit}) {
    commit(REINIT_CLAIMS, [])
}

export async function fetchClaims({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/claims',
            params: {
                // page: 4
            }
        })
        .then(response => {
            commit(FETCH_CLAIMS, response.data);
            return response.data.data
        })
        .catch(error => {
            throw error
        })
}

export async function searchClaims({commit}, {params}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/claims',
            params
        })
        .then(response => {
            commit(FETCH_CLAIMS, response.data);
            return response.data.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchClaim({commit}, {claimID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/claims/' + claimID,
        })
        .then(response => {
            commit(FETCH_CLAIM, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createClaim({commit}, {claim}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/claims',
            data: claim
        })
        .then(response => {
            commit(CREATE_CLAIM, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateClaim({commit}, {claim}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/claims/' + claim.id,
            data: claim
        })
        .then(response => {
            commit(UPDATE_CLAIM, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteClaim({commit}, { claimID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/claims/' + claimID
        })
        .then(response => {
            commit(DELETE_CLAIM, claimID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveClaim({commit, state}, { claim }) {
    const index = state.all.findIndex((x) => x.id === claim.id);
    if (index !== -1 ) {
        return updateClaim({commit}, {claim})
    }
    return createClaim({commit}, {claim})
}
