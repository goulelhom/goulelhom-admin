import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_THEMES,
    FETCH_THEMES,
    FETCH_THEME,
    CREATE_THEME,
    UPDATE_THEME,
    DELETE_THEME
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_THEMES] (state, themes) {
        state.all = initialState.all
    },

    [FETCH_THEMES] (state, themes) {
        state.all = themes
    },

    [FETCH_THEME] (state, theme) {
        const index = state.all.findIndex(x => x.id === theme.id);
        if (index === -1) {
            state.all.push(theme)
        } else {
            state.all.splice(index, 1, theme)
        }
    },

    [CREATE_THEME] (state, theme) {
        state.all.push(theme)
    },

    [UPDATE_THEME] (state, theme) {
        const index = state.all.findIndex(x => x.id === theme.id);
        if (index !== -1) {
            state.all.splice(index, 1, theme)
        }
    },

    [DELETE_THEME] (state, themeID) {
        state.all = state.all.filter(x => x.id !== themeID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
