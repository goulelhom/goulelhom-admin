export const REINIT_THEMES = 'claims/themes/REINIT_THEMES';
export const FETCH_THEMES = 'claims/themes/FETCH_THEMES';
export const FETCH_THEME = 'claims/themes/FETCH_THEME';
export const CREATE_THEME = 'claims/themes/CREATE_THEME';
export const UPDATE_THEME = 'claims/themes/UPDATE_THEME';
export const DELETE_THEME = 'claims/themes/DELETE_THEME';
