import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_THEMES,
    FETCH_THEMES,
    FETCH_THEME,
    CREATE_THEME,
    UPDATE_THEME,
    DELETE_THEME
} from './mutation-types'

export async function reinitThemes({commit}) {
    commit(REINIT_THEMES, [])
}

export async function fetchThemes({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/themes',
        })
        .then(response => {
            commit(FETCH_THEMES, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchTheme({commit}, {themeID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/themes/' + themeID,
        })
        .then(response => {
            commit(FETCH_THEME, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createTheme({commit}, {theme}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/themes',
            data: theme
        })
        .then(response => {
            commit(CREATE_THEME, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateTheme({commit}, {theme}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/themes/' + theme.id,
            data: theme
        })
        .then(response => {
            commit(UPDATE_THEME, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteTheme({commit}, { themeID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/themes/' + themeID
        })
        .then(response => {
            commit(DELETE_THEME, themeID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveTheme({commit, state}, { theme }) {
    const index = state.all.findIndex((x) => x.id === theme.id);
    if (index !== -1 ) {
        return updateTheme({commit}, {theme})
    }
    return createTheme({commit}, {theme})
}
