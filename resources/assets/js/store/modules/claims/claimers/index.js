import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_CLAIMERS,
    FETCH_CLAIMERS,
    FETCH_CLAIMER,
    CREATE_CLAIMER,
    UPDATE_CLAIMER,
    DELETE_CLAIMER
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_CLAIMERS] (state, claimers) {
        state.all = initialState.all
    },

    [FETCH_CLAIMERS] (state, claimers) {
        state.all = claimers
    },

    [FETCH_CLAIMER] (state, claimer) {
        const index = state.all.findIndex(x => x.id === claimer.id);
        if (index === -1) {
            state.all.push(claimer)
        } else {
            state.all.splice(index, 1, claimer)
        }
    },

    [CREATE_CLAIMER] (state, claimer) {
        state.all.push(claimer)
    },

    [UPDATE_CLAIMER] (state, claimer) {
        const index = state.all.findIndex(x => x.id === claimer.id);
        if (index !== -1) {
            state.all.splice(index, 1, claimer)
        }
    },

    [DELETE_CLAIMER] (state, claimerID) {
        state.all = state.all.filter(x => x.id !== claimerID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
