export const REINIT_CLAIMERS = 'claims/claimers/REINIT_CLAIMERS';
export const FETCH_CLAIMERS = 'claims/claimers/FETCH_CLAIMERS';
export const FETCH_CLAIMER = 'claims/claimers/FETCH_CLAIMER';
export const CREATE_CLAIMER = 'claims/claimers/CREATE_CLAIMER';
export const UPDATE_CLAIMER = 'claims/claimers/UPDATE_CLAIMER';
export const DELETE_CLAIMER = 'claims/claimers/DELETE_CLAIMER';
