import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_EVENTS,
    FETCH_EVENTS,
    FETCH_EVENT,
    CREATE_EVENT,
    UPDATE_EVENT,
    DELETE_EVENT
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_EVENTS] (state, events) {
        state.all = initialState.all
    },

    [FETCH_EVENTS] (state, events) {
        state.all = events
    },

    [FETCH_EVENT] (state, event) {
        const index = state.all.findIndex(x => x.id === event.id);
        if (index === -1) {
            state.all.push(event)
        } else {
            state.all.splice(index, 1, event)
        }
    },

    [CREATE_EVENT] (state, event) {
        state.all.push(event)
    },

    [UPDATE_EVENT] (state, event) {
        const index = state.all.findIndex(x => x.id === event.id);
        if (index !== -1) {
            state.all.splice(index, 1, event)
        }
    },

    [DELETE_EVENT] (state, eventID) {
        state.all = state.all.filter(x => x.id !== eventID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
