import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_EVENTS,
    FETCH_EVENTS,
    FETCH_EVENT,
    CREATE_EVENT,
    UPDATE_EVENT,
    DELETE_EVENT
} from './mutation-types'

export async function reinitEvents({commit}) {
    commit(REINIT_EVENTS, [])
}

export async function fetchEvents({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/events',
        })
        .then(response => {
            commit(FETCH_EVENTS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchPublicEvents({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/public/events',
        })
        .then(response => {
            commit(FETCH_EVENTS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchEvent({commit}, {eventID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/events/' + eventID,
        })
        .then(response => {
            commit(FETCH_EVENT, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createEvent({commit}, {event}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/events',
            data: event
        })
        .then(response => {
            commit(CREATE_EVENT, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateEvent({commit}, {event}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/events/' + event.id,
            data: event
        })
        .then(response => {
            commit(UPDATE_EVENT, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteEvent({commit}, { eventID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/events/' + eventID
        })
        .then(response => {
            commit(DELETE_EVENT, eventID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveEvent({commit, state}, { event }) {
    const index = state.all.findIndex((x) => x.id === event.id);
    if (index !== -1 ) {
        return updateEvent({commit}, {event})
    }
    return createEvent({commit}, {event})
}
