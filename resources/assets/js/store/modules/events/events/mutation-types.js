export const REINIT_EVENTS = 'events/events/REINIT_EVENTS';
export const FETCH_EVENTS = 'events/events/FETCH_EVENTS';
export const FETCH_EVENT = 'events/events/FETCH_EVENT';
export const CREATE_EVENT = 'events/events/CREATE_EVENT';
export const UPDATE_EVENT = 'events/events/UPDATE_EVENT';
export const DELETE_EVENT = 'events/events/DELETE_EVENT';
