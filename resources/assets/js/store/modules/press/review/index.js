import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_REVIEWS,
    FETCH_REVIEWS,
    FETCH_REVIEW,
    CREATE_REVIEW,
    UPDATE_REVIEW,
    DELETE_REVIEW
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_REVIEWS] (state, reviews) {
        state.all = initialState.all
    },

    [FETCH_REVIEWS] (state, reviews) {
        state.all = reviews
    },

    [FETCH_REVIEW] (state, review) {
        const index = state.all.findIndex(x => x.id === review.id);
        if (index === -1) {
            state.all.push(review)
        } else {
            state.all.splice(index, 1, review)
        }
    },

    [CREATE_REVIEW] (state, review) {
        state.all.push(review)
    },

    [UPDATE_REVIEW] (state, review) {
        const index = state.all.findIndex(x => x.id === review.id);
        if (index !== -1) {
            state.all.splice(index, 1, review)
        }
    },

    [DELETE_REVIEW] (state, reviewID) {
        state.all = state.all.filter(x => x.id !== reviewID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
