export const REINIT_REVIEWS = 'press/reviews/REINIT_REVIEWS';
export const FETCH_REVIEWS = 'press/reviews/FETCH_REVIEWS';
export const FETCH_REVIEW = 'press/reviews/FETCH_REVIEW';
export const CREATE_REVIEW = 'press/reviews/CREATE_REVIEW';
export const UPDATE_REVIEW = 'press/reviews/UPDATE_REVIEW';
export const DELETE_REVIEW = 'press/reviews/DELETE_REVIEW';
