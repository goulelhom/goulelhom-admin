import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_REVIEWS,
    FETCH_REVIEWS,
    FETCH_REVIEW,
    CREATE_REVIEW,
    UPDATE_REVIEW,
    DELETE_REVIEW
} from './mutation-types'

export async function reinitReviews({commit}) {
    commit(REINIT_REVIEWS, [])
}

export async function fetchReviews({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/reviews',
        })
        .then(response => {
            commit(FETCH_REVIEWS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchPublicReviews({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/public/reviews',
        })
        .then(response => {
            commit(FETCH_REVIEWS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchReview({commit}, {reviewID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/reviews/' + reviewID,
        })
        .then(response => {
            commit(FETCH_REVIEW, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createReview({commit}, {review}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/reviews',
            data: review
        })
        .then(response => {
            commit(CREATE_REVIEW, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateReview({commit}, {review}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/reviews/' + review.id,
            data: review
        })
        .then(response => {
            commit(UPDATE_REVIEW, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteReview({commit}, { reviewID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/reviews/' + reviewID
        })
        .then(response => {
            commit(DELETE_REVIEW, reviewID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveReview({commit, state}, { review }) {
    const index = state.all.findIndex((x) => x.id === review.id);
    if (index !== -1 ) {
        return updateReview({commit}, {review})
    }
    return createReview({commit}, {review})
}
