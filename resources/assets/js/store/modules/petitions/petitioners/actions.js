import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_CLAIMERS,
    FETCH_CLAIMERS,
    FETCH_CLAIMER,
    CREATE_CLAIMER,
    UPDATE_CLAIMER,
    DELETE_CLAIMER
} from './mutation-types'

export async function reinitClaimers({commit}) {
    commit(REINIT_CLAIMERS, [])
}

export async function fetchClaimers({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/claimers',
        })
        .then(response => {
            commit(FETCH_CLAIMERS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchClaimer({commit}, {claimerID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/claimers/' + claimerID,
        })
        .then(response => {
            commit(FETCH_CLAIMER, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createClaimer({commit}, {claimer}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/claimers',
            data: claimer
        })
        .then(response => {
            commit(CREATE_CLAIMER, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateClaimer({commit}, {claimer}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/claimers/' + claimer.id,
            data: claimer
        })
        .then(response => {
            commit(UPDATE_CLAIMER, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteClaimer({commit}, { claimerID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/claimers/' + claimerID
        })
        .then(response => {
            commit(DELETE_CLAIMER, claimerID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveClaimer({commit, state}, { claimer }) {
    const index = state.all.findIndex((x) => x.id === claimer.id);
    if (index !== -1 ) {
        return updateClaimer({commit}, {claimer})
    }
    return createClaimer({commit}, {claimer})
}
