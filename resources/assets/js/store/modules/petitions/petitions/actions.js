import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_PETITIONS,
    FETCH_PETITIONS,
    FETCH_PETITION,
    CREATE_PETITION,
    UPDATE_PETITION,
    DELETE_PETITION
} from './mutation-types'

export async function reinitPetitions({commit}) {
    commit(REINIT_PETITIONS, [])
}

export async function fetchPetitions({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/petitions',
            params: {
                // page: 4
            }
        })
        .then(response => {
            commit(FETCH_PETITIONS, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function searchPetitions({commit}, {params}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/petitions',
            params
        })
        .then(response => {
            commit(FETCH_PETITIONS, response.data);
            return response.data.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchPetition({commit}, {petitionID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/petitions/' + petitionID,
        })
        .then(response => {
            commit(FETCH_PETITION, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createPetition({commit}, {petition}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/petitions',
            data: petition
        })
        .then(response => {
            commit(CREATE_PETITION, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updatePetition({commit}, {petition}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/petitions/' + petition.id,
            data: petition
        })
        .then(response => {
            commit(UPDATE_PETITION, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deletePetition({commit}, { petitionID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/petitions/' + petitionID
        })
        .then(response => {
            commit(DELETE_PETITION, petitionID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function savePetition({commit, state}, { petition }) {
    const index = state.all.findIndex((x) => x.id === petition.id);
    if (index !== -1 ) {
        return updatePetition({commit}, {petition})
    }
    return createPetition({commit}, {petition})
}
