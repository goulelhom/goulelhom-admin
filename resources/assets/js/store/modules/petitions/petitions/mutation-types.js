export const REINIT_PETITIONS = 'petitions/petitions/REINIT_PETITIONS';
export const FETCH_PETITIONS = 'petitions/petitions/FETCH_PETITIONS';
export const FETCH_PETITION = 'petitions/petitions/FETCH_PETITION';
export const CREATE_PETITION = 'petitions/petitions/CREATE_PETITION';
export const UPDATE_PETITION = 'petitions/petitions/UPDATE_PETITION';
export const DELETE_PETITION = 'petitions/petitions/DELETE_PETITION';
