import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_PETITIONS,
    FETCH_PETITIONS,
    FETCH_PETITION,
    CREATE_PETITION,
    UPDATE_PETITION,
    DELETE_PETITION
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_PETITIONS] (state, petitions) {
        state.all = initialState.all
    },

    [FETCH_PETITIONS] (state, petitions) {
        state.all = petitions
    },

    [FETCH_PETITION] (state, petition) {
        const index = state.all.findIndex(x => x.id === petition.id);
        if (index === -1) {
            state.all.push(petition)
        } else {
            state.all.splice(index, 1, petition)
        }
    },

    [CREATE_PETITION] (state, petition) {
        state.all.push(petition)
    },

    [UPDATE_PETITION] (state, petition) {
        const index = state.all.findIndex(x => x.id === petition.id);
        if (index !== -1) {
            state.all.splice(index, 1, petition)
        }
    },

    [DELETE_PETITION] (state, petitionID) {
        state.all = state.all.filter(x => x.id !== petitionID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
