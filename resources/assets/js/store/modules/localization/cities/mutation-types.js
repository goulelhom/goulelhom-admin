export const REINIT_CITIES = 'localization/cities/REINIT_CITIES';
export const FETCH_CITIES = 'localization/cities/FETCH_CITIES';
export const FETCH_CITY = 'localization/cities/FETCH_CITY';
export const CREATE_CITY = 'localization/cities/CREATE_CITY';
export const UPDATE_CITY = 'localization/cities/UPDATE_CITY';
export const DELETE_CITY = 'localization/cities/DELETE_CITY';
