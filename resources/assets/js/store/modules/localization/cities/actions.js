import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_CITIES,
    FETCH_CITIES,
    FETCH_CITY,
    CREATE_CITY,
    UPDATE_CITY,
    DELETE_CITY
} from './mutation-types'

export function reinitCities({commit}) {
    commit(REINIT_CITIES, [])
}

export async function fetchCities({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/cities',
        })
        .then(response => {
            commit(FETCH_CITIES, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchCity({commit}, {cityID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/cities/' + cityID,
        })
        .then(response => {
            commit(FETCH_CITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createCity({commit}, {city}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/cities',
            data: city
        })
        .then(response => {
            commit(CREATE_CITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateCity({commit}, {city}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/cities/' + city.id,
            data: city
        })
        .then(response => {
            commit(UPDATE_CITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteCity({commit}, { cityID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/cities/' + cityID
        })
        .then(response => {
            commit(DELETE_CITY, cityID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveCity({commit, state}, { city }) {
    const index = state.all.findIndex((x) => x.id === city.id);
    if (index !== -1 ) {
        return updateCity({commit}, {city})
    }
    return createCity({commit}, {city})
}
