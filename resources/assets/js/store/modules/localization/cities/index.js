import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_CITIES,
    FETCH_CITIES,
    FETCH_CITY,
    CREATE_CITY,
    UPDATE_CITY,
    DELETE_CITY
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_CITIES] (state, cities) {
        state.all = initialState.all
    },

    [FETCH_CITIES] (state, cities) {
        state.all = cities
    },

    [FETCH_CITY] (state, city) {
        const index = state.all.findIndex(x => x.id === city.id);
        if (index === -1) {
            state.all.push(city)
        } else {
            state.all.splice(index, 1, city)
        }
    },

    [CREATE_CITY] (state, city) {
        state.all.push(city)
    },

    [UPDATE_CITY] (state, city) {
        const index = state.all.findIndex(x => x.id === city.id);
        if (index !== -1) {
            state.all.splice(index, 1, city)
        }
    },

    [DELETE_CITY] (state, cityID) {
        state.all = state.all.filter(x => x.id !== cityID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
