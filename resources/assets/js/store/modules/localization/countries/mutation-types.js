export const REINIT_COUNTRIES = 'localization/countries/REINIT_COUNTRIES';
export const FETCH_COUNTRIES = 'localization/countries/FETCH_COUNTRIES';
export const FETCH_COUNTRY = 'localization/countries/COUNTRY';
export const CREATE_COUNTRY = 'localization/countries/CREATE_COUNTRY';
export const UPDATE_COUNTRY = 'localization/countries/UPDATE_COUNTRY';
export const DELETE_COUNTRY = 'localization/countries/DELETE_COUNTRY';
