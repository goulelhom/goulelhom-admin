import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_COUNTRIES,
    FETCH_COUNTRIES,
    FETCH_COUNTRY,
    CREATE_COUNTRY,
    UPDATE_COUNTRY,
    DELETE_COUNTRY
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_COUNTRIES] (state, countries) {
        state.all = initialState.all
    },

    [FETCH_COUNTRIES] (state, countries) {
        state.all = countries
    },

    [FETCH_COUNTRY] (state, country) {
        const index = state.all.findIndex(x => x.id === country.id);
        if (index === -1) {
            state.all.push(country)
        } else {
            state.all.splice(index, 1, country)
        }
    },

    [CREATE_COUNTRY] (state, country) {
        state.all.push(country)
    },

    [UPDATE_COUNTRY] (state, country) {
        const index = state.all.findIndex(x => x.id === country.id);
        if (index !== -1) {
            state.all.splice(index, 1, country)
        }
    },

    [DELETE_COUNTRY] (state, countryID) {
        state.all = state.all.filter(x => x.id !== countryID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
