import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_COUNTRIES,
    FETCH_COUNTRIES,
    FETCH_COUNTRY,
    CREATE_COUNTRY,
    UPDATE_COUNTRY,
    DELETE_COUNTRY
} from './mutation-types'

export async function reinitCountries({commit}) {
    commit(REINIT_COUNTRIES, [])
}

export async function fetchCountries({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/countries',
        })
        .then(response => {
            commit(FETCH_COUNTRIES, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchCountry({commit}, {countryID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/countries/' + countryID,
        })
        .then(response => {
            commit(FETCH_COUNTRY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createCountry({commit}, {country}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/countries',
            data: country
        })
        .then(response => {
            commit(CREATE_COUNTRY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateCountry({commit}, {country}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/countries/' + country.id,
            data: country
        })
        .then(response => {
            commit(UPDATE_COUNTRY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteCountry({commit}, { countryID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/countries/' + countryID
        })
        .then(response => {
            commit(DELETE_COUNTRY, countryID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveCountry({commit, state}, { country }) {
    const index = state.all.findIndex((x) => x.id === country.id);
    if (index !== -1 ) {
        return updateCountry({commit}, {country})
    }
    return createCountry({commit}, {country})
}
