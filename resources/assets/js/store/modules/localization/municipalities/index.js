import * as actions from './actions'
import * as getters from './getters'

import {
    REINIT_MUNICIPALITIES,
    FETCH_MUNICIPALITIES,
    FETCH_MUNICIPALITY,
    CREATE_MUNICIPALITY,
    UPDATE_MUNICIPALITY,
    DELETE_MUNICIPALITY
} from './mutation-types'

const initialState = {
    all: []
};

const mutations = {

    [REINIT_MUNICIPALITIES] (state, municipalities) {
        state.all = initialState.all
    },

    [FETCH_MUNICIPALITIES] (state, municipalities) {
        state.all = municipalities
    },

    [FETCH_MUNICIPALITY] (state, municipality) {
        const index = state.all.findIndex(x => x.id === municipality.id);
        if (index === -1) {
            state.all.push(municipality)
        } else {
            state.all.splice(index, 1, municipality)
        }
    },

    [CREATE_MUNICIPALITY] (state, municipality) {
        state.all.push(municipality)
    },

    [UPDATE_MUNICIPALITY] (state, municipality) {
        const index = state.all.findIndex(x => x.id === municipality.id);
        if (index !== -1) {
            state.all.splice(index, 1, municipality)
        }
    },

    [DELETE_MUNICIPALITY] (state, municipalityID) {
        state.all = state.all.filter(x => x.id !== municipalityID)
    }
};

export default {
    state: { ...initialState },
    actions,
    getters,
    mutations
}
