import axios from 'axios'

import {apiDomain as apiUrl} from '../../../../conf'

import {
    REINIT_MUNICIPALITIES,
    FETCH_MUNICIPALITIES,
    FETCH_MUNICIPALITY,
    CREATE_MUNICIPALITY,
    UPDATE_MUNICIPALITY,
    DELETE_MUNICIPALITY
} from './mutation-types'

export function reinitMunicipalities({commit}) {
    commit(REINIT_MUNICIPALITIES, [])
}

export async function fetchMunicipalities({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/municipalities',
        })
        .then(response => {
            commit(FETCH_MUNICIPALITIES, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchPublicMunicipalities({commit}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/public/municipalities',
        })
        .then(response => {
            commit(FETCH_MUNICIPALITIES, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchMunicipality({commit}, {municipalityID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/municipalities/' + municipalityID,
        })
        .then(response => {
            commit(FETCH_MUNICIPALITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function fetchPublicMunicipality({commit}, {municipalityID}) {
    return axios(
        {
            method: 'GET',
            url: apiUrl + '/public/municipalities/' + municipalityID,
        })
        .then(response => {
            commit(FETCH_MUNICIPALITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function createMunicipality({commit}, {municipality}) {
    return axios(
        {
            method: 'POST',
            url: apiUrl + '/municipalities',
            data: municipality
        })
        .then(response => {
            commit(CREATE_MUNICIPALITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function updateMunicipality({commit}, {municipality}) {
    return axios(
        {
            method: 'PUT',
            url: apiUrl + '/municipalities/' + municipality.id,
            data: municipality
        })
        .then(response => {
            commit(UPDATE_MUNICIPALITY, response.data);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function deleteMunicipality({commit}, { municipalityID }) {
    return axios(
        {
            method: 'DELETE',
            url: apiUrl + '/municipalities/' + municipalityID
        })
        .then(response => {
            commit(DELETE_MUNICIPALITY, municipalityID);
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export async function saveMunicipality({commit, state}, { municipality }) {
    const index = state.all.findIndex((x) => x.id === municipality.id);
    if (index !== -1 ) {
        return updateMunicipality({commit}, {municipality})
    }
    return createMunicipality({commit}, {municipality})
}
