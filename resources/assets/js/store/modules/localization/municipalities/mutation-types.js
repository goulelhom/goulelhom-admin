export const REINIT_MUNICIPALITIES = 'localization/municipalities/REINIT_MUNICIPALITIES';
export const FETCH_MUNICIPALITIES = 'localization/municipalities/FETCH_MUNICIPALITIES';
export const FETCH_MUNICIPALITY = 'localization/municipalities/FETCH_MUNICIPALITY';
export const CREATE_MUNICIPALITY = 'localization/municipalities/CREATE_MUNICIPALITY';
export const UPDATE_MUNICIPALITY = 'localization/municipalities/UPDATE_MUNICIPALITY';
export const DELETE_MUNICIPALITY = 'localization/municipalities/DELETE_MUNICIPALITY';
